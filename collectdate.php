<style>

h4 {
    font-weight: 600;
    color: #415093;
    line-height: 24px;
    font-size: 21px;
    margin: 0 0 20px;
}

.radio-box label {
    margin: 0 0 0 25px;
    display: inline-block;
    vertical-align: top;
    line-height: 17px;
    font-size: 15px;
    color: #222;
    font-weight: normal;
}

.bg-grey {
    background: #f8f8ff;
}

.offer-step.condition-inner {
    padding: 50px 100px;
}

.condition-inner {
    max-width: 700px;
    margin: 0px auto 70px;
    background: #fff;
    box-shadow: 0 -6px 57px 0 rgba(65,80,147,.24);
    border-radius: 12px;
    padding: 50px;
}

 .offer-step .rb-box {
    max-width: 280px;
    margin: 0 auto 10px;
}

.rb-box {
    margin-bottom: 25px;
}

.price-accept {
    text-align: center;
    margin: 0 0 60px;
}

.gnh-box {
    margin: 0 0 30px;
}

.text-center {
    text-align: center!important;
}

.offer-p {
    margin: 0;
    opacity: .5;
}

.form-radio:checked {
    background-color: #11c47e !important;
    border: none;
}

.offer-step.condition-inner {
    padding: 50px 100px;
}

.your-order-box {
    max-width: 700px;
    background: #fff;
    padding: 70px;
    border-radius: 12px;
    margin: 90px auto 60px;
    box-shadow: 0 -6px 57px 0 rgba(65,80,147,.24);
}

.your-order-box h3 {
    text-align: center;
    margin-bottom: 30px;
    font-weight: bold;
    color: #363639;
    line-height: 28px;
    font-size: 24px;
    margin: 0 0 15px;
}

input[type="text"], input[type="search"], input[type="email"], input[type="password"], textarea, input[type="date"] {

    width: 100%;
    border: 2px solid #415093;
    border-radius: 20px;
    height: 38px;
    line-height: 34px;
    padding: 0 15px;
    margin: 0;
    color: #415093;
    font-size: 12px;
    background: #fff;
    outline: none;
    font-size: 14px;
}

.your-order-box .input-box {
    padding: 6px;
}

.input-box.left {
    float: left;
    width: 48.5%;
}

.input-box {
    margin: 0 0 30px;
}

.your-order-box .col-md-4 .green-link {
    white-space: nowrap;
    margin-top: 32px;
    margin-left: -10px;
}

.your-order-box .col-md-4 .button {
    white-space: nowrap;
    margin-top: 25px;
    height: 40px;
    line-height: 40px;
    padding: 0 30px;
}

.your-order-box .red-box {
    margin-bottom: 40px;
}

.red-box {
    background: #fdf3ec;
    border: 1px solid #e5dbd3;
    border-radius: 8px;
    margin: 0 0 25px;
    padding: 15px 15px 15px 60px;
    position: relative;
}

.red-box span {
    position: absolute;
    background: #f87012;
    line-height: 24px;
    height: 24px;
    width: 24px;
    text-align: center;
    color: #fff;
    font-weight: bold;
    font-style: italic;
    border-radius: 24px;
    left: 15px;
    top: 12px;
}

button[type="submit"], .button {
    box-shadow: 0 5px 29px 0 #84ffcf;
    background: #11c47e;
    border: none;
    color: #fff;
    border-radius: 30px;
    line-height: 60px;
    height: 60px;
    padding: 0 30px 0 70px;
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: 1.52px;
    cursor: pointer;
    display: inline-block;
    outline: none;
}

button[type="submit"]:hover, .button:hover {
    box-shadow: 0 5px 29px 0 #415093;
    background: #415093;
    outline: none;
}

button[type="submit"] span, .button span {
    padding-left: 30px;
}

.form-radio:checked::before {
    position: absolute;
    left: 6px;
    top: 0;
    content: '\02143';
    transform: rotate(40deg);
    color: #fff;
    font-weight: bold;
    font-size: 10px;
}

.green-link {
    display: inline-block;
    vertical-align: middle;
    margin: 0px;
    font-size: 16px;
    color: #11c47e;
    text-decoration: underline;
    font-weight: 600;
    cursor: pointer;
    background: none;
    padding: 0;
}

.green-link img, .white-link img {
    margin: 0 0 0 7px;
    width: 15px;
}

button.green-link, button.white-link {
    border: none;
}

.gnh-box {
    margin: 0 0 30px;
}

.offer-step .gnh-box .radio-box {
    margin: 0 15px 0 0;
}

.radio-box {
    margin: 0 35px 15px 0;
    line-height: 100%;
    display: inline-block;
    position: relative;
}

.form-radio {
     -webkit-appearance: none; 
    position: absolute;
    background: #fff;
    top: 0px;
    left: 0;
    height: 17px;
    width: 17px;
    border-radius: 50px;
    cursor: pointer;
    outline: none;
    border: 1px solid #dadada;
    line-height: 17px;
}

.price {
    font-size: 56px;
    line-height: 56px;
    color: #415093;
    font-weight: 600;
    margin: 0 0 30px;
}

.mod_btn
{
    box-shadow: 0 5px 29px 0 #84ffcf;
    background: #11c47e;
    padding: 20px;
    border-radius: 30px;
    color: #fff;
    border: none;
}

.mod_btn:hover
{
    box-shadow: 0 5px 29px 0 #415093;
    background: #415093;
    color:#fff;
    text-decoration: none !important;
}

</style>

<?php ini_set('display_errors', 0);
include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
    <div class="condition-top-section">
        <div class="container">
          <div class="row">    
             <div class="col-12">
                <h1 id="progress-bar-header" class="text-center">Collection Date</h1>
                <div id="progress-bar" class="row condition-progress-bar text-center">
                   <div class="col-3 cp-box first cp-box-active js-active cp-box-done">
                      <span>Your details</span>
                      <div class="tick"></div>
                   </div>
                   <div class="col-3 cp-box">
                      <span>Payment Method</span>
                      <div class="tick"></div>
                   </div>
                   <div class="col-3 cp-box cp-box-process cp-box-active js-active">
                      <span>Collection Date</span>
                      <div class="tick"></div>
                   </div>
                   <div class="col-3 cp-box last">
                      <span>Confirm</span>
                      <div class="tick"></div>
                   </div>
                </div>
             </div>
          </div>
        </div>
    </div>

    <div class="condition-top-section">
       <div class="container">
            <div class="row">
                <div class="col-12">
                <form id="checkout-step-form" class="d-none"> <input type="hidden" name="checkout_step"> 
                </form>
                <div id="checkout-content" class="your-order-box">
                   <h3>Hardware Pickup Details</h3>
                   
                   <?php
                            $name=$_POST["name"];
                            $mdlm=$_POST['mdl_name'];
                            $mdlp0=$_POST['mdl_prs0'];
                            $mdlp1=$_POST['mdl_prs1'];
                            $mdlp=$_POST['mdl_prs'];
                            $mdls=$_POST['mdl_scr'];
                            $mdna=$_POST['mdl_mod'];
                            $modn=$_POST['model_name'];
                            $ser=$_POST['serial_number'];
                            $radio=$_POST['power'];
                            $mac_condition_yes=$_POST['mac_condition_yes'];
                            $mac_condition_no=$_POST['mac_condition_no'];
                            $price=$_POST['price'];
                            $rdio=$_POST['work_adap'];
                            $Mobile_Number=$_POST["mobile"];
                            $Email=$_POST["email"];
                            $payment_type=$_POST["payment_type"];
                            $paypal_email=$_POST["paypal_email"];
                            
                            $bank_account_holder=$_POST["bank_account_holder"];
                            $bank_account_number=$_POST["bank_account_number"];
                            $bank_sort_code_1=$_POST["bank_sort_code_1"];
                            $bank_sort_code_2=$_POST["bank_sort_code_2"];
                            $bank_sort_code_3=$_POST["bank_sort_code_3"];
                            
                            $mac_ram = $_POST['mac_ram'];
                            $ramoth = $_POST['ramoth'];
                            $mac_storage = $_POST['mac_storage'];
                            $storageoth = $_POST['storageoth'];
                                  
                            //echo $mac_ram,$ramoth,$mac_storage,$storageoth; exit;
                            
                            $deviceprice = $_POST['deviceprice'];
                            $thumbnailUrl = $_POST['thumbnailUrl'];
                            $model = $_POST['model'];
                            $item = $_POST['item'];
                            $pro_type = $_POST['pro_type'];
                            $modelNumber = $_POST["modelNumber"];
                                                  
                        ?>
                        
                   <form action="confirm.php" class="account-form" method="post">
                      <input type="hidden" name="country" value="United Kingdom"> 
                      <div class="d-flex">
                        <div class="input-box d-flex flex-column col-md-12"> 
                            <input type="hidden" name="mdl_name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mdl_prs0" value="<?php echo $mdlp0; ?>">
                            <input type="hidden" name="mdl_prs1" value="<?php echo $mdlp1; ?>">
                            <input type="hidden" name="mdl_prs" value="<?php echo $mdlp; ?>">
                            <input type="hidden" name="mdl_scr" value="<?php echo $mdls; ?>">
                            <input type="hidden" name="mdl_mod" value="<?php echo $mdna; ?>">
                            <input type="hidden" name="model_name" value="<?php echo $modn; ?>">
                            <input type="hidden" name="serial_number" value="<?php echo $ser; ?>">
                            <input type="hidden" name="power" value="<?php echo $radio; ?>">
                            <input type="hidden" name="mac_condition_yes" value="<?php echo $mac_condition_yes; ?>">
                            <input type="hidden" name="mac_condition_no" value="<?php echo $mac_condition_no; ?>">
                            <input type="hidden" name="price" value="<?php echo $price ?>">
                            <input type="hidden" name="work_adap" value="<?php echo $rdio; ?>">
                            
                            <input type="hidden" name="name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mobile" value="<?php echo $Mobile_Number ?>">
                            <input type="hidden" name="email" value="<?php echo $Email; ?>">
                            <input type="hidden" name="payment_type" value="<?php echo $payment_type; ?>">
                            <input type="hidden" name="paypal_email" value="<?php echo $paypal_email; ?>">
                            
                            <input type="hidden" name="bank_account_holder" value="<?php echo $bank_account_holder; ?>">
                            <input type="hidden" name="bank_account_number" value="<?php echo $bank_account_number; ?>">
                            <input type="hidden" name="bank_sort_code_1" value="<?php echo $bank_sort_code_1; ?>">
                            <input type="hidden" name="bank_sort_code_2" value="<?php echo $bank_sort_code_2; ?>">
                            <input type="hidden" name="bank_sort_code_3" value="<?php echo $bank_sort_code_3; ?>">
                            
                            <input type="hidden" name="mac_ram" value="<?php echo $mac_ram; ?>">
                            <input type="hidden" name="ramoth" value="<?php echo $ramoth; ?>">
                            <input type="hidden" name="mac_storage" value="<?php echo $mac_storage; ?>">
                            <input type="hidden" name="storageoth" value="<?php echo $mac_storage; ?>">
                            
                            <input type="hidden" name="deviceprice" value="<?php echo $deviceprice; ?>">
                            <input type="hidden" name="item" value="<?php echo $item; ?>">
                            <input type="hidden" name="model" value="<?php echo $model; ?>">
                            <input type="hidden" name="thumbnailUrl" value="<?php echo $thumbnailUrl; ?>">
                            <input type="hidden" name="pro_type" value="<?php echo $pro_type; ?>">
                            <input type="hidden" name="modelNumber" value="<?php echo $modelNumber; ?>">
                            <input type="hidden" name="modelIdentifier" value="<?php echo $modelIdentifier; ?>">
                            <input type="hidden" name="standardRAM" value="<?php echo $mac_ram; ?>">
                            <input type="hidden" name="standardHD" value="<?php echo $mac_storage; ?>">
                        
                            <p>Please select the date we can pick up your gadgets
                               <img src="images/question.png" alt="Help" onmouseover="bigImg(this)" onmouseout="normalImg(this)">
                            </p>
                            
                            <div class="row">
                              <div class="col-md-4 col-sm-4 pr-md-2"></div>
                              <div class="col-md-4 col-sm-4 pr-md-2">
                                <div class="input-box"><input type="text" id="datepick" name="colldatetime"></div>
                              </div>
                              <div class="col-md-4 col-sm-4 pr-md-2"></div>
                            </div>
                            
                            <p>Please enter the address your gadget will be collected from</p>
                        

                        <div class="row">
                            <div class="col-md-6 col-sm-6 pr-md-2">
                             <div class="input-box"> <label>House name/number</label> <input type="text" name="address1" value="" placeholder="House name/number"> </div>
                             <div class="input-box"> <label>Street</label> <input type="text" name="address2" value="" placeholder="Street"> </div>
                            </div>
                            
                            <div class="col-md-6 col-sm-6 pr-md-2">
                             <div class="input-box"> <label>Town/City</label> <input type="text" name="city" value="" placeholder="Town/City"> </div>
                             <div class="input-box"> <label>Postcode</label> <input type="text" name="postcode" value="" placeholder="Postcode"> </div>
                            </div>
                        </div>
                        
                        </div>
                      </div>
                      <div class="clear"></div>
                      <div class="submit-box"> 
                      <button type="submit" class="js-help-modal mod_btn" id="mod_btn" rel="help" data-max-width="800px" data-target="#js-offer-modal" data-toggle="modal" data-backdrop="static" data-keyboard="false">Continue<span>&gt;</span></button>
                      </div>
                   </form>
                </div>
                </div>
            </div>
        </div>
    </div>

</section> 

<?php include 'layouts/footer.php';?>

<script>
      $(document).ready(function() {
        $('#datepick').datepicker({
                  showButtonPanel:true,
                  timeOnly:false,
                  timeOnlyShowDate:false,
                  showHour:null,
                  showMinute:null,
                  showTimezone:null,
                  showTime:true,
                  hour: 0,
                  minute: 0,
                  second: 0,
                  timezone:null,
                  hourMin: 0,
                  minuteMin: 0,
                  secondMin: 0,
                  hourMax: 23,
                  minuteMax: 59,
                  secondMax: 59,
                  minDateTime:null,
                  maxDateTime:null,
                  maxTime:null,
                  minTime:null,
                  hourGrid: 0,
                  minuteGrid: 0,
                  secondGrid: 0,
                  separator:' ',
                  pickerTimeFormat:null,
                  pickerTimeSuffix:null,
                  showTimepicker:true,
            });
      });
</script>
  
<script>
 
//On click of the 'next' anchor
$('.accordion--form__next-btn').on('click touchstart', function() {

  var parentWrapper = $(this).parent().parent();
  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');
  var sectionFields = $(this).siblings().find('.required');


  //Validate the .required fields in this section
  var empty = $(this).siblings().find('.required').filter(function() {

    return this.value === "";

  });

  if (empty.length) {

    $('.accordion--form__invalid').show();

  } else {

    $('.accordion--form__invalid').hide();

    //If valid
    //On the next fieldset -> accordion wrapper, toggle the active class
    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

    //Close the others
    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

    //Add a class to the parent legend
    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

    //Remove the active class from the other legends
    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');

  }

  return false;
});

//On click of the 'prev' anchor
$('.accordion--form__prev-btn').on('click touchstart', function() {

  parentWrapper = $(this).parent().parent();
  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');

  //On the prev fieldset -> accordion wrapper, toggle the active class
  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

  //Close the others
  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

  //Add a class to the parent legend
  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

  //Remove the active class from the other legends
  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');


  return false;
});
    
       function switchVisible() {
            if (document.getElementById('Selection-1-container')) {

                if (document.getElementById('Selection-1-container').style.display == 'none') {
                    document.getElementById('Selection-1-container').style.display = 'block';
                     document.getElementById('Selection-2-container').style.display = 'none';
                  
                }
                else {
                    document.getElementById('Selection-2-container').style.display = 'none';
                }
            }
}

  function switchVisible1() {
            if (document.getElementById('Selection-2-container')) {

                if (document.getElementById('Selection-2-container').style.display == 'none') {
                    document.getElementById('Selection-2-container').style.display = 'block';
                    document.getElementById('Selection-1-container').style.display = 'none';

                  
                }
                else {
                    document.getElementById('Selection-1-container').style.display = 'none';
                }
            }
}
    function bigImg() {
  $('.suggestion-box').show();
}

function normalImg() {
     $('.suggestion-box').hide();
}
</script>
</body>
</html>