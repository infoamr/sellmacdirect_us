<?php 
ini_set('display_errors', 0);
include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
  <div class="condition-top-section">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <h1 class="mb-4">Condition &amp; Functionality</h1>
            <p style="font-size: 16px;">In order to offer an accurate price we need to know the age and specification of your macbook</p>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
               <div id="progress-bar" class="row condition-progress-bar">
                  <div class="col-3 cp-box first">
                     <span style="font-size: 16px;">Select OS</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 16px;">Select Product</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box cp-box-process cp-box-active js-active">
                     <span style="font-size: 16px;">Condition &amp; Functionality</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box last">
                     <span style="font-size: 16px;">Confirm offer</span>
                     <div class="tick"></div>
                  </div>
               </div>
            </div>
        </div>
    </div>
  </div>

<div class="container">

<div class="row">
    <div id="offer-content" class="condition-inner condition-step">

<?php
include 'layouts/connection.php';

$mac = $_GET['mac'];
$sqlmod = "SELECT * from macbook_ver where id ='$mac'";
$rs = $conn->query($sqlmod);
if ($rs->num_rows > 0) 
{
    while($row = $rs->fetch_assoc()) 
    {   
        $modelname= $row['name'];
    }
}

$macpr = isset($_GET['mac_pr'])?$_GET['mac_pr']:$_GET['final2'];
$sqlpro = "SELECT * from macbook_prs where id ='$macpr'";
$rspro = $conn->query($sqlpro);
if ($rspro->num_rows > 0) 
{
    while($rowpr = $rspro->fetch_assoc()) 
    {   
        $proname= $rowpr['name'];
    }
}
?>
   <h6 class="text-left text-uppercase"> <?php echo $modelname; ?> </h6>
   <h6 class="text-left text-uppercase"><?php echo $proname; ?> </h6>
   <form method="post" action="offer.php" name="yourForm" id="theForm" >

      <input type="hidden" name="mac_prod" value="<?php echo $_GET['mac']; ?>">     <!-- MacBook Version -->  
      <input type="hidden" name="mac_prs" value="<?php echo $_GET['mac_pr']; ?>">   <!-- MacBook Version -->  
      <input type="hidden" name="macinte" value="<?php echo $_GET['final2']; ?>">   <!-- MacBook Version -->  
      <input type="hidden" name="mac_ospr" value="<?php echo $_GET['mac_os']; ?>">  <!-- MacBook prosessor -->  
      <input type="hidden" name="mac_srn" value="<?php echo $_GET['macscr']; ?>">   <!-- MacBook Screen -->  
      <input type="hidden" name="mac_mdn" value="<?php echo $_GET['macmod']; ?>">   <!-- MacBook Model -->  
      <input type="hidden" name="devicemodel" value="<?php echo $modelname; ?>">    <!-- MacBook Version -->  
      <label for="Selection-1" class="w-100">

        <hr>
        <div class="form-group text-left">  
            <div class="col-md-12 rb-box process">
                <p style="font-size: 14px;"><b>Does your macbook power on ?</b></p>
                <div class="radio-box">
                    <input type="radio" name="Selection" id="Selection-1" value="yes" class="form-radio js-modifier-selector" onclick="switchVisible()" required>
                    <label for="Selection-1" class="w-100">Yes</label>
                </div>

                <div class="radio-box">
                    <input type="radio" name="Selection" id="Selection-2" value="no" class="form-radio js-modifier-selector" onclick="switchVisible1()"required>
                    <label for="Selection-2" class="w-100">No</label>
                </div>
            </div>
        </div>

        <div id="Selection-1-container" style="display:none;">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook turns on when the power button is pressed. It produces an image on the screen and will remain on indefinitely when the mains power is connected.</p>
            </div>
            <hr>
            <div class="form-group text-left">
                <div class="col-md-6 rb-box process">
                  <p style="font-size: 14px;"><b>My macbook's condition is</b></p>
                  <select class="form-control required" name="mac" onchange="showDiv(this)">
                    <option selected disabled ><span style="font-size: 14px;">Select your macbook condition</span></option>
                    <option value="1" name="Selection" id="Selection-3"><span style="font-size: 14px;">Good Condition</span></option>
                    <option value="2" name="Selection" id="Selection-3"><span style="font-size: 14px;">Single Point Of Damage</span></option>
                    <option value="3" name="Selection" id="Selection-3"><span style="font-size: 14px;">Faulty / Damaged Screen</span></option>
                    <option value="4" name="Selection" id="Selection-3"><span style="font-size: 14px;">Multiple Points Of Damage</span></option>
                  </select>
                </div>
            </div>
        </div>

        <div id="Selection-2-container" style="display:none" >
            <!-- code changed here too, added the class -->
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook fails to turn on &amp; produce an image on the screen when the power button is pressed or turns off randomly even when connected to the mains.</p>
            </div>
            <hr>
            <div class="form-group text-left">
                <div class="col-md-6 rb-box process">
                  <p style="font-size: 14px;"><b>My macbook's condition is</b></p>
                  <select class="form-control" name="mac2"  onchange="showDiv1(this)">
                    <option selected disabled ><span style="font-size: 14px;">Select your macbook condition</span></option>
                    <option value="5" name="Selection"><span style="font-size: 14px;">Good</span></option>
                    <option value="6" name="Selection"><span style="font-size: 14px;">Damaged</span></option>
                  </select>
                </div>
            </div>
        </div>

        <div id="hidden_div">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook is in good condition for a used item, it doesn’t have to be perfect, but it can’t have any non-working parts or noticeable damage.</p>
                <a style="font-size: 14px;" class="green-link js-help-modal" href="" data-max-width="460px" data-target="#js-click1" data-toggle="modal">Click Here For Further Info</a>
            </div>
        </div>

        <div id="hidden_div1" style="display:none;">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook has a single part which is damaged or faulty.</p>
                <a style="font-size: 14px;" class="green-link js-click" href=" " data-max-width="460px" data-target="#js-click2" data-toggle="modal">Click Here For Further Info</a>
            </div>
        </div>

        <div id="hidden_div2" style="display:none;">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook is otherwise in good condition but has a screen imperfection, such as visible screen scratches, a line running down the screen or staining visible when the screen is turned off</p>
                <a style="font-size: 14px;" class="green-link js-help-modal" href="" data-max-width="460px" data-target="#js-click3" data-toggle="modal">Click Here For Further Info</a>
            </div>
        </div>

        <div id="hidden_div3" style="display:none;">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook has 2 or more points of damage or faults. For example a non-working keyboard and a crack to its case.</p>
                <a style="font-size: 14px;" class="green-link js-help-modal" href="" data-max-width="460px" data-target="#js-click4" data-toggle="modal">Click Here For Further Info</a>
            </div>
        </div>

        <div id="hidden_div5" style="display:none;">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook is in good condition for a used item, it doesn’t have to be perfect, but it can’t have any noticeable damage.</p>
                <a style="font-size: 14px;" class="green-link js-help-modal" href="" data-max-width="460px" data-target="#js-click5" data-toggle="modal">Click Here For Further Info</a>
            </div>
        </div>

        <div id="hidden_div6" style="display:none;">
            <div class="expanded-box-inner rb-box text-left">
                <p style="font-size: 14px;">The MacBook has any part which is damaged.</p>
                <a style="font-size: 14px;" class="green-link js-help-modal" href="" data-max-width="460px" data-target="#damaged" data-toggle="modal">Click Here For Further Info</a>
            </div>
        </div>

        <hr>
        <div class="form-group text-left">  
            <div class="col-md-12 rb-box process">
                <p style="font-size: 14px;"><b>Do you have a working AC power adapter ?</b> <br></p>
                <div class="radio-box">
                    <input type="radio" name="Select" value="yes" id="modifier-518" class="form-radio js-modifier-selector"> <label for="modifier-518" class="w-100">Yes</label> </div>
                <div class="radio-box">
                    <input type="radio" name="Select" value="no" id="modifier-519" class="form-radio js-modifier-selector"> <label for="modifier-519" class="w-100">No</label> </div>
                <p><a style="font-size: 14px;" class="green-link js-help-modal" href="" data-max-width="460px" data-target="#ac-power" data-toggle="modal">What does this mean?</a></p>
            </div>
        </div>
 
        <hr>
        <!-- <div class="row"> -->
          <div class="form-group text-left">
            <div class="col-md-6 rb-box process">
              <p style="font-size: 14px;"><b>RAM Size</b></p>
              <select class="form-control required" name="mac_ram" id="mac_ram">
                <option selected disabled style="font-size: 14px" >Select size</option>
                <option value="4GB" style="font-size: 14px" name="Selection">4 GB</option>
                <option value="8GB" style="font-size: 14px" name="Selection">8 GB</option>
                <option value="16GB" style="font-size: 14px" name="Selection">16 GB</option>
                <option value="32GB" style="font-size: 14px" name="Selection">32 GB</option>
                <option value="64GB" style="font-size: 14px" name="Selection">64 GB</option>
                <option value="Other" style="font-size: 14px" name="Selection">Other</option>
              </select>
            </div>
          </div>

          <div class="form-group text-left" id="ramoth" style="display:none";>
            <div class="col-md-6 rb-box process">
                <input type="text" name="ramoth" maxlength="8" placeholder="enter ram size"/>
            </div>
          </div>
        <!-- </div> -->

        <!-- <div class="row"> -->
          <div class="form-group text-left">
            <div class="col-md-6 rb-box process">
               <p style="font-size: 14px;"><b>Storage Capacity</b></p>
               <select class="form-control required" name="mac_storage" id="mac_storage">
                    <option selected disabled><span style="font-size: 14px;">Select capacity</span></option>
                    <option value="512GB" name="Selection"><span style="font-size: 14px;">512 GB</span>
                    </option>
                    <option value="1 TB"  name="Selection"><span style="font-size: 14px;">1 TB</span></option>
                    <option value="Other" name="Selection"><span style="font-size: 14px;">Other</span></option>
               </select>
            </div>
          </div>

          <div class="form-group text-left" id="storageoth" style="display:none;">
            <div class="col-md-6 rb-box process">
                <input type="text" name="storageoth" maxlength="8" placeholder="enter storage capacity"/>
            </div>
          </div>
        <!-- </div> -->

        <div class="text-center">
            <button type="submit" class="button"value="submit">continue <span>&gt;</span></button> 
        </div>

   </form>

  </div>    

 </div>

</div>

<div class="modal fade" id="js-click1" role="dialog" tabindex="-1" aria-hidd="true">

    <div class="modal-dialog modal-dialog-centered" style="max-width:500px !important;">

      <!-- Modal content-->

      <div class="modal-content" style="margin-top:120px;">

        <div class="modal-body position-relative" style="overflow-y: auto;height: 300px;">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

         <div class="container-fluid">

   <div class="row mx-0">

        <div class="col-auto mb-5"> 

        <p> All of the below must be true: </p>

        <ul> <li>Screen functions normal</li>

        <li>Screen image is clear of dead pixels or bright spots</li> 

        <li>Screen is clear of marks when switched off (no staining)</li>

        <li>All parts function normally</li> <li>No cracks or missing parts</li>

        <li>Light scratching and wear is acceptable</li> <li>Battery health is normal</li> 

        </ul> <p></p> </div>

      </div>

   </div>

</div>

        </div>

      </div>

    </div>

    <div class="modal fade" id="js-click2" role="dialog" tabindex="-1" aria-hidd="true">

    <div class="modal-dialog">
      <!-- Modal content-->

      <div class="modal-content" style="margin-top:120px;">

        <div class="modal-body position-relative" style="overflow-y: auto;height: 500px;">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

         <div class="container-fluid">

   <div class="row mx-0">

      <div class="col-auto mb-5"> <p> The MacBook can have one of the following points of damage: </p>

      <ul> <li>Non-working optical drive</li> <li>Crack to plastics</li> <li>Significant or deep scratches</li> 

      <li>Missing or non-working keys</li> <li>Faulty trackpad or buttons</li> <li>No battery present</li>

      <li>Up to 3 dead pixels</li> <li>1 small bright patch of screen</li> <li>Faulty webcam</li>

      <li>Battery replacement recommend by Mac OS</li> </ul> <p></p> <p>If the MacBook has 2 or more of the following 

      issues it may be classed as having Multiple points of damage.</p> </div>

      </div>

   </div>

</div>

        </div>

      

      </div>

      

    </div>



    <div class="modal fade" id="js-click3" role="dialog" tabindex="-1" aria-hidd="true">

    <div class="modal-dialog modal-dialog-centered" style="max-width:500px !important;">

    

      <!-- Modal content-->

      <div class="modal-content">

        <div class="modal-body position-relative" style="overflow-y: auto;height: 243px;">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

         <div class="container-fluid">

   <div class="row mx-0">

    <div class="col-auto mb-5"> <p> The MacBook has any of the following damage: </p><ul> <li>Smashed screen</li> <li>Lines down screen</li> <li>Multiple dead pixels</li> <li>Bright white patches of screen</li> <li>Screen staining visible when the screen is switched off</li> </ul> <p></p> </div>

      </div>

   </div>

</div>

        </div>

      

      </div>

      

    </div>

      <div class="modal fade" id="js-click4" role="dialog" tabindex="-1" aria-hidd="true">

    <div class="modal-dialog" style="max-width:500px;">

    

      <!-- Modal content-->

      <div class="modal-content" style="margin-top:120px;">

        <div class="modal-body position-relative" style="overflow-y: auto;height: 450px;">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

         <div class="container-fluid">

   <div class="row mx-0">

<div class="col-auto mb-5"> <p> A MacBook with two or more of the below issues will be considered “heavily damaged” </p><ul> <li>Non-working optical drive</li> <li>Crack to plastics</li> <li>Significant or deep scratches</li> <li>Missing or non-working keys</li> <li>Faulty trackpad or buttons</li> <li>No battery present</li> <li>Faulty webcam</li> <li>Missing parts</li> <li>Battery replacement recommend by Mac OS</li> <li>Swollen battery</li> <li>Up to 3 dead pixels</li> <li>1 small bright patch of screen</li> <li>Any Physical damage to the screen</li> <li>Lines running down the screen</li> </ul> <p></p> </div>      </div>

   </div>

</div>

        </div>

      

      </div>

      

    </div>



<div class="modal fade" id="js-click5" role="dialog" tabindex="-1" aria-hidd="true">

    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 460px !important;">

    

      <!-- Modal content-->

      <div class="modal-content">

        <div class="modal-body position-relative">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

         <div class="container-fluid">

   <div class="row mx-0">

      <div class="mb-5">

        <div class="col-auto mb-5"> <p> All of the below must be true: </p>

        <ul> <li>The screen must be undamaged and functional</li>

        <li>All parts function normally</li> <li>No cracks or missing parts</li> 

        <li>Light scratching and wear is acceptable</li> </ul> <p></p> </div>

      </div>

   </div>

</div>

        </div>

      

      </div>

      

    </div>

  </div>

<div class="modal fade" id="ac-power" role="dialog" tabindex="-1" aria-hidd="true">
    
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="margin-top:120px;">
        <div class="modal-body position-relative" style="overflow-y: auto; height: 336px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            <div class="container-fluid">
               <div class="row mx-0">
                  <div class="mb-5">
                     <h1 class="mb-4">What is a working AC power adapter ?</h1>
                     <p>You can find your devices serial number by one of the following methods.</p>
                     <p>The A/C adaptor and the lead connecting it to the Macbook should not be damaged (Wires should not be frayed and the adaptor block should not be cracked).</p>
                     <p>Your A/C adaptor can be for any region but must be orignal or a CE approved replacement charger.</p>
                  </div>
               </div>
            </div>
        </div>
    </div>
      
    </div>
</div>

<div class="modal fade" id="damaged" role="dialog" tabindex="-1" aria-hidd="true">
    
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="margin-top:120px;">
        <div class="modal-body position-relative" style="overflow-y: auto; height: 336px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            <div class="container-fluid">
               <div class="row mx-0">
                  <div class="mb-5">
                     <p>Your non-working MacBook will be classed as “damaged” if it has any of the following:</p>
                     <ul> 
                        <li>Any Screen damage including staining</li>
                        <li>Crack to plastics</li>
                        <li>Significant or deep scratches</li>
                        <li>Missing or non-working keys</li>
                        <li>Missing parts</li>
                        <li>Swollen battery</li>
                    </ul>
                  </div>
               </div>
            </div>
        </div>
    </div>
      
    </div>
</div>

</section> 

<?php include 'layouts/footer.php';?>

<script>

    $(document).ready(function(){

        $("#mac_ram").change(function(){

            $(this).find("option:selected").each(function(){

                var optionValue = $(this).attr("value");

                if(optionValue == 'Other'){

                    $("#ramoth").show();

                } else{

                    $("#ramoth").hide();

                }

            });

        }).change();

    });

    $(document).ready(function(){

        $("#mac_storage").change(function(){

            $(this).find("option:selected").each(function(){

                var optionValue = $(this).attr("value");

                if(optionValue == 'Other'){

                    $("#storageoth").show();

                } else{

                    $("#storageoth").hide();

                }

            });

        }).change();

    });

</script>

<script>

//On click of the 'next' anchor

$('.accordion--form__next-btn').on('click touchstart', function() {



  var parentWrapper = $(this).parent().parent();

  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');

  var sectionFields = $(this).siblings().find('.required');





  //Validate the .required fields in this section

  var empty = $(this).siblings().find('.required').filter(function() {



    return this.value === "";



  });



  if (empty.length) {



    $('.accordion--form__invalid').show();



  } else {



    $('.accordion--form__invalid').hide();



    //If valid

    //On the next fieldset -> accordion wrapper, toggle the active class

    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');



    //Close the others

    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');



    //Add a class to the parent legend

    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');



    //Remove the active class from the other legends

    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');



  }



  return false;

});



//On click of the 'prev' anchor

$('.accordion--form__prev-btn').on('click touchstart', function() {



  parentWrapper = $(this).parent().parent();

  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');



  //On the prev fieldset -> accordion wrapper, toggle the active class

  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');



  //Close the others

  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');



  //Add a class to the parent legend

  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');



  //Remove the active class from the other legends

  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');





  return false;

});



// $(document).ready(function() {

//     	$('input[name="Selection"]').click(function() {



//           $('.togglehid').addClass('hidden');

//           // code changed here --> add the class hidden to all div's with class togglehid



//       	if($(this).attr('id') == 'Selection-1')

//         {

//         	$("#Selection-1-container").toggleClass('hidden');

//         }

//         else if($(this).attr('id') == 'Selection-2')

//         {

//         	$("#Selection-2-container").toggleClass('hidden');

//         }

        

//       });

//     });

    

function switchVisible() {

    if (document.getElementById('Selection-1-container')) {

        if (document.getElementById('Selection-1-container').style.display == 'none') {

            document.getElementById('Selection-1-container').style.display = 'block';

            document.getElementById('Selection-2-container').style.display = 'none';
        }
        else {
            document.getElementById('Selection-2-container').style.display = 'none';
        }
    }
}

function switchVisible1() {
    if (document.getElementById('Selection-2-container')) {



        if (document.getElementById('Selection-2-container').style.display == 'none') {

            document.getElementById('Selection-2-container').style.display = 'block';

            document.getElementById('Selection-1-container').style.display = 'none';



          

        }

        else {

            document.getElementById('Selection-1-container').style.display = 'none';

        }

    }
}

function showDiv(select){

   if(select.value==1){

    document.getElementById('hidden_div').style.display = "block";

    document.getElementById('hidden_div1').style.display = "none";

    document.getElementById('hidden_div2').style.display = "none";  

    document.getElementById('hidden_div3').style.display = "none";

   } 

   else if(select.value==2)

   {

       document.getElementById('hidden_div').style.display = "none";

    document.getElementById('hidden_div2').style.display = "none";  

    document.getElementById('hidden_div3').style.display = "none";

    document.getElementById('hidden_div1').style.display = "block";

     document.getElementById('hidden_div5').style.display = "none";

    document.getElementById('hidden_div6').style.display = "none";

   }

   else if(select.value==3)

   {

    document.getElementById('hidden_div').style.display = "none";

    document.getElementById('hidden_div1').style.display = "none";

    document.getElementById('hidden_div3').style.display = "none";

    document.getElementById('hidden_div2').style.display = "block";

     document.getElementById('hidden_div5').style.display = "none";

    document.getElementById('hidden_div6').style.display = "none";

    

   }

   else if(select.value==4)

   {

     document.getElementById('hidden_div').style.display = "none";

    document.getElementById('hidden_div1').style.display = "none";

    document.getElementById('hidden_div2').style.display = "none";  

    document.getElementById('hidden_div3').style.display = "block";

     document.getElementById('hidden_div5').style.display = "none";

    document.getElementById('hidden_div6').style.display = "none";

   }

   

   else

   {

       document.getElementById('hidden_div').style.display = "none";

       document.getElementById('hidden_div1').style.display = "none";

    document.getElementById('hidden_div2').style.display = "none";  

    document.getElementById('hidden_div3').style.display = "none";

     document.getElementById('hidden_div5').style.display = "none";

    document.getElementById('hidden_div6').style.display = "none";

       

   }

} 

function showDiv1(select){

   if(select.value==5){

    document.getElementById('hidden_div5').style.display = "block";

    document.getElementById('hidden_div6').style.display = "none";

     document.getElementById('hidden_div').style.display = "none";

       document.getElementById('hidden_div1').style.display = "none";

    document.getElementById('hidden_div2').style.display = "none";  

    document.getElementById('hidden_div3').style.display = "none";

   } 

   else if(select.value==6) {
        document.getElementById('hidden_div5').style.display = "none";

        document.getElementById('hidden_div6').style.display = "block";

        document.getElementById('hidden_div').style.display = "none";

        document.getElementById('hidden_div1').style.display = "none";

        document.getElementById('hidden_div2').style.display = "none";  

        document.getElementById('hidden_div3').style.display = "none";

   }

   else {
       document.getElementById('hidden_div5').style.display = "none";

       document.getElementById('hidden_div6').style.display = "none";

       document.getElementById('hidden_div').style.display = "none";

       document.getElementById('hidden_div1').style.display = "none";

       document.getElementById('hidden_div2').style.display = "none";  

       document.getElementById('hidden_div3').style.display = "none";
   }

}

</script>