<?php 
ini_set('display_errors', 0);
$toEmail = "sales@sellmacdirect.co.uk";

$name = $_POST["name"];
$subject = $_POST["subject"];
$email = $_POST["email"];
$message = $_POST["msgbody"];

if ($_POST['g-recaptcha-response'] == '') { ?>
 <script language="javascript" type="text/javascript">
    alert('Captcha field is required');
  window.location.href = 'https://sellmacdirect.co.uk/contact.php';
 </script>
 <?php } ?>
    
<?php

$sub = 'Call Back';
$msgbody = 'Thank you for contacting us.<br>We will get back to you shortly.';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';

require_once "vendor/autoload.php";

//PHPMailer Object
$mail = new PHPMailer(true); 

//From email address and name
$mail->From = $toEmail;
$mail->FromName = 'SellMacDirect';

//To address and name
$mail->addAddress($email); //Recipient name is optional

//Send HTML or Plain Text email
$mail->isHTML(true);

$mail->Subject = $sub;
$mail->Body = $msgbody;

try {
    $mail->send();
    //echo "Message has been sent successfully";
} catch (Exception $e) {
    //echo "Mailer Error: " . $mail->ErrorInfo;
}

$htmlContent = ' 
    <html> 
    <head> 
        <title>Sell Mac Direct</title> 
    </head> 
    <body> 
        <h1>Contact Page Enquiry!</h1> 
        <table cellspacing="0" style="border: 2px dashed #FB4314; width: 100%;">
            <tr> 
                <th>Name:</th><td>'.$name.'</td> 
            </tr> 
            <tr style="background-color: #e0e0e0;"> 
                <th>Email:</th><td>'.$email.'</td> 
            </tr> 
            <tr style="background-color: #e0e0e0;"> 
                <th>Message:</th><td>'.$message.'</td> 
            </tr> 
        </table> 
    </body> 
    </html>'; 

// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$name.'<'.$email.'>' . "\r\n"; 
// $headers .= 'Cc: welcome@example.com' . "\r\n"; 
// $headers .= 'Bcc: welcome2@example.com' . "\r\n"; 

$mail_status = mail($toEmail,$subject,$htmlContent,$headers);

if ($mail_status) { ?>
 <script language="javascript" type="text/javascript">
  window.location.href = 'https://sellmacdirect.co.uk/contact_thankyou.php';
 </script>
 <?php
 } else { ?>
  <script language="javascript" type="text/javascript">
// alert('Message sending failed');
   window.location.href = 'https://sellmacdirect.co.uk/';
  </script>
<?php } ?>