<?php include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
  <div class="condition-top-section">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <h1 class="mb-4" style="none">It’s easy to sell your Macbook for cash!</h1>
            <p style="font-size: 18px;">In order to offer an accurate price we need to know the age and specification of your macbook</p>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
               <div id="progress-bar" class="row condition-progress-bar">
                  <div class="col-3 cp-box cp-box-process cp-box-active js-active first">
                     <span style="font-size: 18px;">Select Type</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Select Generation</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Condition &amp; Functionality</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box last">
                     <span style="font-size: 18px;">Confirm offer</span>
                     <div class="tick"></div>
                  </div>
               </div>
            </div>
        </div>
    </div>
  </div>


<section class="flex-grow-1 d-flex flex-column">
 <div class="buttons condition-box">
    <div class="container">
     <div class="row">
       <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
          <h4 class="mb-4">Select Your Mac Pro Type</h4>
          <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
             <span>How do I find this</span>
             <picture>
                <img src="images/question.png" alt="Help"> 
             </picture>
          </button>
          <br><br>
       </div>
    </div>
<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_ver where mac_id='pro'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo "<div class='container'>";
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
     if($row['id']=='6')
     {
        echo "<a class='show box best' target='2' data-s_id='{$row['id']}'>";
        echo "<div class='box-image'>";
        echo "<img src='images/macbook.png'>";
        echo "</div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
     }
     elseif($row['id']=='7')
     {
        echo " <a  class='show box best1' target='3' data-s_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/macbook.png'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
     }
     elseif($row['id']=='8')
     {
        echo " <a  class='show box best2' target='4' data-s_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/macbook.png'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
     }
     else
     {
        echo " <a  class='show box box1' target='1' data-s_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/macbook.png'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";

    }
    }
    echo "</div>";
    echo "</div>";
    
} ?>

</div>
</div>
<div class="container">
<div id="div1" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Silver Keyboard Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
               <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class='row justify-content-center'>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_prs WHERE id='10' ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row

    while($row = $result->fetch_assoc()) {
        
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
        echo " <form  action='intelprocess.php'  class='box' id='my1Form'>";
        echo " <div class='box-image' onclick='my1Function()'>";
        echo " <img src='images/intelcore2.png'  alt='Macbook Intel Core i2'>";
        echo "</div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo  "<input type='hidden' name='mac' id='mac_nm'/>";
        echo  "<input type='hidden' name='mac_os' value='".$row['id']."'/>";
        echo "</p>";
        echo "</form>";
        echo "</a>";
        echo "</div>";
     
    }}
    echo "</div>";
?>
</div>
</div>
</div>
<div class="container">
<div id="div2" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Optical Drive Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-target="#js-help-modal" data-toggle="modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="row justify-content-center">
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_prs ";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if($row['id']==8)
        {
        echo "<div class='col-xl-2 col-lg-2 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
        echo " <a class='show2 box fixed' target='5' data-q_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei7.png' alt='Macbook Pro Intel Core i7'>";
        echo " </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";

    }
    if($row['id']==9)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed' target='5' data-q_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei5.png'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
    }
    if($row['id']==10)
    {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed' target='5' data-q_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcore2.png'  alt='Macbook Intel Core i2'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
    }
        
    }
    echo "</div>";

} ?>

</div>
</div>
</div>
<div class="container">
<div id="div3" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro Retina Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal">
            <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="row justify-content-center">
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_prs ";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if($row['id']==8)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <a class='show2 box fixedm' target='6' data-f_id='{$row['id']}'>";
      echo " <div class='box-image'>";
          echo " <img src='images/intelcorei7.png' alt='Macbook Intel Core i5'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                 echo "</p>";
            
    echo "</a>";
    echo "</div>";
     
    }
         if($row['id']==9)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <a  class='show2 box fixedm1' target='11' data-f_id='{$row['id']}'>";
      echo " <div class='box-image'>";
          echo " <img src='images/intelcorei5.png' alt='Macbook Intel Core i5'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                 echo "</p>";
            
    echo "</a>";
    echo "</div>";
     
    }
    }
    echo "</div>";

} ?>


</div>
</div>
</div>
<div class="container">
<div id="div4" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";;

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_prs order by id DESC";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==11)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <a class='show2 box fixed4' target='8' data-f_id='{$row['id']}' >";
      echo " <div class='box-image'>";
          echo " <img src='images/intelcorei9.png'  alt='Macbook Intel Core i5'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p>";
                 echo "</p>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==9)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <a class='show2 box fixed5' target='9' data-f_id='{$row['id']}' >";
      echo " <div class='box-image'>";
          echo " <img src='images/intelcorei5.jpeg'  alt='Macbook Intel Core i5'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p>";
                 echo "</p>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==8)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <a class='show2 box fixed6' target='10' data-f_id='{$row['id']}'>";
      echo " <div class='box-image'>";
          echo " <img src='images/intelcorei7.jpeg'  alt='Macbook Intel Core i5'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                 echo "</p>";
            
    echo "</a>";
    echo "</div>";
     
    }
        
    }
    echo "</div>";

} ?>

</div>
</div>   
</div>
<div class="container">
<div id="div5" class="targetDivs">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Optical Drive Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
    
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_size order by id desc";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==5)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my2Form'>";
      echo " <div class='box-image' onclick='my2Function()'>";
          echo " <img src='images/macbook-pro-17.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_r1'/>";
               echo  "<input type='hidden' name='mac_os' id='mac_osr'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==3)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my3Form'>";
      echo " <div class='box-image' onclick='my3Function()'>";
          echo " <img src='images/macbook-pro-15.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_r2'/>";
               echo  "<input type='hidden' name='mac_os' id='mac_osrr'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==2)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my4Form'>";
      echo " <div class='box-image' onclick='my4Function()'>";
          echo " <img src='images/macbook-pro-13.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                 echo  "<input type='hidden' name='mac' id='mac_r3'/>";
               echo  "<input type='hidden' name='mac_os' id='mac_osrrr'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
            
    echo "</a>";
    echo "</div>";
     
    }
        
    }
    echo "</div>";

}
?>

</div>
</div>
<div class="container">
<div id="div6" class="targetDivs">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro Retina Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
    
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <a class='show2 box sized' target='7' data-size_id='{$row['id']}'>";
      echo " <div class='box-image'>";
          echo " <img src='images/macbook-pro-13.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==3)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my5Form'>";
      echo " <div class='box-image' onclick='my5Function()'>";
          echo " <img src='images/macbook-pro-15.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_r6'/>";
               echo  "<input type='hidden' name='mac_os' id='mac_osrr1'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
        
    }
    echo "</div>";

}
?>

</div>
</div>
<div class="container">
<div id="div7" class="targetDivs">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro Retina Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
               <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
    
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo "<div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
    if($row['id']==1)
    {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo "<form  action='intelprocess.php' class='box' id='my6Form'>";
        echo "<div class='box-image' onclick='my6Function()'>";
        echo "<img src='images/image10.png' alt='model number'>";
        echo "</div>";
        echo "<p>".trim($row['name'])."</p> ";
        echo "<input type='hidden' name='mac' id='mac_r5'/>";
        echo "<input type='hidden' name='mac_os' id='mac_osrr3'/>";
        echo "<input type='hidden' name='macscr' id='mac_res'/>";
        echo "<input type='hidden' name='macmod' value='".$row['id']."'/>";
        echo "</p>";
        echo "</form>";
        echo "</div>";
    }
    if($row['id']==2)
    {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo "<form  action='intelprocess.php'  class='box' id='my7Form'>";
        echo "<div class='box-image' onclick='my7Function()'>";
        echo "<img src='images/image10.png'  alt='model number'>";
        echo "</div>";
        echo "<p>".trim($row['name'])."</p> ";
        echo "<input type='hidden' name='mac' id='mac_r7'/>";
        echo "<input type='hidden' name='mac_os' id='mac_osrr4'/>";
        echo "<input type='hidden' name='macscr' id='mac_res1'/>";
        echo "<input type='hidden' name='macmod' value='".$row['id']."'/>";
        echo "</p>";
        echo "</form>";
        echo "</div>";
    }
        
    }
    echo "</div>";
} ?>

</div>
<div class="container">
<div id="div11" class="targetDivs">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro Retina Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
    
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==1)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php' class='box' id='my8Form'>";
      echo " <div class='box-image' onclick='my8Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_r8'/>";
               echo  "<input type='hidden' name='mac_os' id='mac_osrr2'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==2)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my9Form'>";
      echo " <div class='box-image' onclick='my9Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_r9'/>";
               echo  "<input type='hidden' name='mac_os' id='mac_osrr5'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
        
    }
    echo "</div>";

} ?>

</div>
<div id="div8" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Sizer</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==3)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my10Form'>";
      echo " <div class='box-image' onclick='my10Function()'>";
          echo " <img src='images/macbook-pro-15.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                  echo  "<input type='hidden' name='mac' id='mac_tch'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==4)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my11Form'>";
      echo " <div class='box-image' onclick='my11Function()'>";
          echo " <img src='images/macbook-pro-16.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_tch1'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os1'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
        
    }
    echo "</div>";

} ?>

</div>
</div>
<div class="container">
<div id="div9" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
    
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my12Form'>";
      echo " <div class='box-image' onclick='my12Function()'>";
          echo " <img src='images/macbook-pro-13.png' alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_tch2'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os2'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
        if($row['id']==3)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my13Form'>";
      echo " <div class='box-image' onclick='my13Function()'>";
          echo " <img src='images/macbook-pro-15.png'  alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                  echo  "<input type='hidden' name='mac' id='mac_tch3'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os3'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==4)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
      echo " <form  action='intelprocess.php'  class='box' id='my14Form'>";
      echo " <div class='box-image' onclick='my14Function()'>";
          echo " <img src='images/macbook-pro-16.png' alt='screen size'>";
           echo" </div>";
                echo "<p>" .$row['size']."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_tch4'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os4'/>";
               echo  "<input type='hidden' name='macscr' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
        
    }
    echo "</div>";

} ?>
 
</div>
</div>
<div class="container">
<div id="div10" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sellmacbook";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==3)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <form  action='intelprocess.php' class='box' id='my15Form'>";
      echo " <div class='box-image' onclick='my15Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_tch8_1'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os8_1'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    if($row['id']==5)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <form  action='intelprocess.php'  class='box' id='my16Form'>";
      echo " <div class='box-image' onclick='my16Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
                echo  "<input type='hidden' name='mac' id='mac_tch8_2'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os8_2'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
     if($row['id']==6)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <form  action='intelprocess.php'  class='box' id='my17Form'>";
      echo " <div class='box-image' onclick='my17Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
               echo  "<input type='hidden' name='mac' id='mac_tch8_3'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os8_3'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
     if($row['id']==7)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <form  action='intelprocess.php'  class='box' id='my18Form'>";
      echo " <div class='box-image' onclick='my18Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
              echo  "<input type='hidden' name='mac' id='mac_tch8_4'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os8_4'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
     if($row['id']==9)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <form  action='intelprocess.php'  class='box' id='my19Form'>";
      echo " <div class='box-image' onclick='my19Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
               echo  "<input type='hidden' name='mac' id='mac_tch8_5'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os8_5'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
     if($row['id']==10)
        {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
      echo " <form  action='intelprocess.php'  class='box' id='my20Form'>";
      echo " <div class='box-image' onclick='my20Function()'>";
          echo " <img src='images/image10.png'  alt='model number'>";
           echo" </div>";
                echo "<p>".trim($row['name'])."</p> ";
               echo  "<input type='hidden' name='mac' id='mac_tch8_6'/>";
               echo  "<input type='hidden' name='mac_os' id='mactc_os8_6'/>";
               echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                 echo "</p>";
                 echo"</form>";
            
    echo "</a>";
    echo "</div>";
     
    }
    
        
    }
    echo "</div>";

} ?>

</div>
</div>
</div>
    <!-- Find Serial Number Pop Up starts -->
    <div class="modal fade" id="js-help-modal" role="dialog" tabindex="-1" aria-hidd="true">
        <div class="modal-dialog">
           <!-- Modal content-->
            <div class="modal-content" style="margin-top:120px;">
                <div class="modal-body position-relative" style="overflow-y: auto;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                <div class="container-fluid">
                   <div class="row mx-0">
                      <div class="mb-5">
                         <h1 class="mb-4">How do I find Serial Number - MACBOOK</h1>
                         <p>You can find your devices serial number by one of the following methods.</p>
                         <h4 class="mt-4">If your device powers on:</h4>
                         <p>1. Click the Apple icon at the top left of the screen.</p>
                         <img src="images/how_to_find.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                         <p>2. Click “About This Mac”</p>
                         <img src="images/convertto.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                         <p>3. Your serial number will be stated within the details window. You can copy the serial by highlighting the text and pressing <kbd><kbd>Cmd</kbd> + <kbd>C</kbd></kbd>.</p>
                         <img src="images/xserial.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I find The Serial Number"> 
                         <h4 class="mt-4">If your device does not power on:</h4>
                         <p>1. On the underside of your mac there will be some text below the device type, check for the text “Serial”, the following numbers will be the device serial number.</p>
                         <img src="images/macbookserial.png" class="img-fluid rounded shadow-sm d-block mx-auto" alt="How Do I find The Serial Number"> 
                      </div>
                   </div>
                </div>
                </div>
            </div>
            <!-- Modal content ends-->  
        </div>
    </div>
    <!-- Find Serial Number Pop Up ends -->
</section> 

<?php include 'layouts/footer.php';?>

<script>
 
//On click of the 'next' anchor
$('.accordion--form__next-btn').on('click touchstart', function() {

  var parentWrapper = $(this).parent().parent();
  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');
  var sectionFields = $(this).siblings().find('.required');


  //Validate the .required fields in this section
  var empty = $(this).siblings().find('.required').filter(function() {

    return this.value === "";

  });

  if (empty.length) {

    $('.accordion--form__invalid').show();

  } else {

    $('.accordion--form__invalid').hide();

    //If valid
    //On the next fieldset -> accordion wrapper, toggle the active class
    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

    //Close the others
    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

    //Add a class to the parent legend
    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

    //Remove the active class from the other legends
    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');

  }

  return false;
});

//On click of the 'prev' anchor
$('.accordion--form__prev-btn').on('click touchstart', function() {

  parentWrapper = $(this).parent().parent();
  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');

  //On the prev fieldset -> accordion wrapper, toggle the active class
  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

  //Close the others
  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

  //Add a class to the parent legend
  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

  //Remove the active class from the other legends
  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');


  return false;
});


function sendContact() {
  var valid;  
  valid = validateContact();
  if(valid) {
    jQuery.ajax({
    url: "contact_mail.php",
    data:'userName='+$("#userName").val()+'&userEmail='+$("#userEmail").val()+'&subject='+$("#userName","#subject","#userEmail").val()+'&message='+$("#content").val()+'&content='+$('#html-content-holder').html(),
    type: "POST",
    success:function(data){
    $("#mail-status").html(data);
    },
    error:function (){}
    });
  }
}

function my1Function() {
  document.getElementById("my1Form").submit();
}
function my2Function() {
  document.getElementById("my2Form").submit();
}
function my3Function() {
  document.getElementById("my3Form").submit();
}
function my4Function() {
  document.getElementById("my4Form").submit();
}
function my5Function() {
  document.getElementById("my5Form").submit();
}
function my6Function() {
  document.getElementById("my6Form").submit();
}
function my7Function() {
  document.getElementById("my7Form").submit();
}
function my8Function() {
  document.getElementById("my8Form").submit();
}
function my9Function() {
  document.getElementById("my9Form").submit();
}
function my10Function() {
  document.getElementById("my10Form").submit();
}
function my11Function() {
  document.getElementById("my11Form").submit();
}
function my12Function() {
  document.getElementById("my12Form").submit();
}
function my13Function() {
  document.getElementById("my13Form").submit();
}
function my14Function() {
  document.getElementById("my14Form").submit();
}
function my15Function() {
  document.getElementById("my15Form").submit();
}
function my16Function() {
  document.getElementById("my16Form").submit();
}
function my17Function() {
  document.getElementById("my17Form").submit();
}
function my18Function() {
  document.getElementById("my18Form").submit();
}
function my19Function() {
  document.getElementById("my19Form").submit();
}function my20Function() {
  document.getElementById("my20Form").submit();
}
$('.targetDiv').hide();
$('.show').click(function () {
    $('.targetDiv').hide();
    $('#div' + $(this).attr('target')).show();
});

$('.targetDivs').hide();
$('.show2').click(function () {
    $('.targetDivs').hide();
    $('#div' + $(this).attr('target')).show();
});

</script>
<script>
$(document).ready(function()  {
  $(".box1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_nm').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed").click(function() {
    var t2 = $(this).data('q_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osr').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed").click(function() {
    var t2 = $(this).data('q_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrr').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed").click(function() {
    var t2 = $(this).data('q_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrrr').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixedm").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrr1').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixedm").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrr3').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixedm").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrr4').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixedm1").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrr2').val(t2);
  },
  
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixedm1").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_osrr5').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r1').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r2').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r3').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r5').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r6').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r7').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r8').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_r9').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".sized").click(function() {
    var t2 = $(this).data('size_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_res').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".sized").click(function() {
    var t2 = $(this).data('size_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_res1').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch').val(t2);
  },
  
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch1').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch2').val(t2);
  },
  
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch3').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch4').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch8_1').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch8_2').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch8_3').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch8_4').val(t2);
  },
 
});
  });
}); 

</script><script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch8_5').val(t2);
  },
  
    
});
  });
}); 

</script><script>
$(document).ready(function()  {
  $(".best2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mac_tch8_6').val(t2);
  },
 
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed4").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed4").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os1').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed5").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os2').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed5").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os3').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed5").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os4').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed6").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os8_1').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed6").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os8_2').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed6").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os8_3').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed6").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os8_4').val(t2);
  },
 
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed6").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os8_5').val(t2);
  },

    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".fixed6").click(function() {
    var t2 = $(this).data('f_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('#mactc_os8_6').val(t2);
  },
  
    
});
  });
}); 

</script>
