<?php include 'layouts/header.php';?>
 
  <section class="ftco-section contact-section" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row justify-content-start mb-5">
        <div class="col-md-12 text-center heading-section heading-section-white ftco-animate">
            <br><br><br><br>
          <h1 class="mb-3">Thank you for contacting us.</h1>
          <h3 class="mb-3">We will get back to you shortly.</h3>
        </div>
      </div>
    </div>
  </section>    

<?php include 'layouts/footer.php';?>