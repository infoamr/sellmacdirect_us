    
  <footer class="ftco-footer ftco-section">
    <div class="container">
      <div class="row mb-0">
        <div class="col-md">
          <div class="ftco-footer-widget mb-0">
            <a class="navbar-brand" href="index.php"><img src="images/sellmacv5foot.png" width="127" alt="Sell_Mac_Direct_Logo"></a>
            <p class="block-23">
              <ul style="list-style:none;margin:0px!important;padding:0px!important">
                <li>
                  <a href="#">We are the leading bulk buyers of used Apple products in United Kingdom, we pay the best price for your devices.</a>
                </li>
              </ul>
            </p>
            <ul class="ftco-footer-social list-unstyled mt-0">
              <li class="ftco-animate"><a href="https://www.facebook.com/sellmacdirect/"><span class="icon-facebook"></span></a></li>
              <li class="ftco-animate"><a href="https://www.linkedin.com/feed/"><span class="icon-linkedin"></span></a></li>
              <li class="ftco-animate"><a href="https://www.instagram.com/sellmac5/"><span class="icon-instagram"></span></a></li>
              <li class="ftco-animate"><a href="https://twitter.com/SellMac5"><span class="icon-twitter"></span></a></li>
            </ul>
          </div>
        </div>
        <!-- <div class="col-md">
          <div class="ftco-footer-widget mb-4 ml-md-4">
            <h2 class="ftco-heading-2">Community</h2>
            <ul class="list-unstyled">
              <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Search Properties</a></li>
              <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>For Agents</a></li>
              <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Reviews</a></li>
              <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>FAQs</a></li>
            </ul>
          </div>
        </div> -->
        <div class="col-md">
          <div class="ftco-footer-widget mb-4 ml-md-4">
            <h2 class="ftco-heading-2">Quick View</h2>
            <ul class="list-unstyled">
              <li><a href="macbook.php"><span class="icon-long-arrow-right mr-2"></span>Macbook</a></li>
              <li><a href="macbookpro.php"><span class="icon-long-arrow-right mr-2"></span>Macbook Pro</a></li>
              <li><a href="macbookair.php"><span class="icon-long-arrow-right mr-2"></span>Macbook Air</a></li>
              <!-- <li><a href="contact.php"><span class="icon-long-arrow-right mr-2"></span>Contact Us</a></li> -->
            </ul>
          </div>
        </div>
        <div class="col-md">
           <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Information</h2>
            <ul class="list-unstyled">
              <li><a href="privacy.php"><span class="icon-long-arrow-right mr-2"></span>Privacy</a></li>
              <!-- <li><a href="recycle.php"><span class="icon-long-arrow-right mr-2"></span>Recycle</a></li> -->
              <li><a href="warranty.php"><span class="icon-long-arrow-right mr-2"></span>Warranty</a></li>
              <li><a href="termsconditions.php"><span class="icon-long-arrow-right mr-2"></span>Terms & Conditions</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md">
          <div class="ftco-footer-widget mb-4">
          	<h2 class="ftco-heading-2">Have a Questions?</h2>
          	<div class="block-23 mb-3">
              <ul>
                <li><a href="#"><span class="icon icon-map-marker"></span>25 Sipson Road, West Drayton, England Postcode:UB7 9DQ</a></li>
                <li><a href="#"><span class="icon icon-phone"></span>02085 7598 50</a></li>
                <li><a href="#"><span class="icon icon-envelope pr-4"></span>sales@sellmacdirect.co.uk</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved |  Powered by <a href="https://amrkernel.com" target="_blank">AMR Kernel</a></p>
        </div>
      </div>
    </div>
  </footer>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <!-- <script src="js/jquery.min.js"></script> -->
  <!-- <script src="js/rrr.js"></script> -->
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js" async defer></script> 
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
  <script>
var recaptcha_response = '';
function submitUserForm() {
    if(recaptcha_response.length == 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">This field is required.</span>';
        return false;
    }
    return true;
}
 
function verifyCaptcha(token) {
    recaptcha_response = token;
    document.getElementById('g-recaptcha-error').innerHTML = '';
}
</script>

<script>
  $(document).ready(function(){
   $(".counter").counterUp({
      delay: 10,
      time: 1200
    });

  });
</script>
    
  </body>
</html>