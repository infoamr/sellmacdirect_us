<?php include 'layouts/header.php';?>
    
<section class="ftco-section contact-section" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41); padding-bottom: 20px!important;">
  <div class="container">
    <div class="row">
       <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
          <h1 class="mb-3 bread">Contact us</h1>
          <h4 class="text-center">If you got any questions <br>please do not hesitate to send us a message</h4>
          <br><br>
       </div>
    </div>

    <div class="row block-12 justify-content-center mb-5">
      <div class="col-md-6 mb-md-5">
        <form action="sendmessage.php" method="post" class="bg-light contact-form">
          <div class="form-group">
            <input style="border: 2px solid #cc001c; border-radius: 20px;" name="name" type="text" class="form-control" placeholder="Your Name" required>
          </div>
          <div class="form-group">
            <input style="border: 2px solid #cc001c; border-radius: 20px;" name="email" type="email" class="form-control" placeholder="Your Email" required>
          </div>
          <div class="form-group">
            <input style="border: 2px solid #cc001c; border-radius: 20px;" name="subject" type="text" class="form-control" placeholder="Subject" required>
          </div>
          <div class="form-group">
            <textarea style="border: 2px solid #cc001c; border-radius: 20px;" name="msgbody" id="" cols="30" rows="7" class="form-control" placeholder="Message" required></textarea>
          </div>
          <div class="g-recaptcha" data-sitekey="6LdccMQbAAAAAOS9eH9-J3Ng_lUma82261vFgYil" data-callback="verifyCaptcha"></div>
          <div id="g-recaptcha-error"></div>
          <br>
          <div class="submit-box"> 
            <button type="submit">Send Message</button> 
          </div>
        </form>
      </div>
      <div class="col-md-6 mb-md-5">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.616547606885!2d-0.46788138479994507!3d51.50190381892693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876721c9c04fd85%3A0x1224fb90e5cb33ba!2s25%20Sipson%20Rd%2C%20West%20Drayton%20UB7%209DQ%2C%20UK!5e0!3m2!1sen!2sin!4v1629475770073!5m2!1sen!2sin" style="border: 2px solid #cc001c;border-radius: 20px; height: 325px; width: 100%;" allowfullscreen="" loading="lazy"></iframe>
      </div>
    </div>

    <div class="row d-flex mb-5 contact-info justify-content-center">
    	<div class="col-md-8">
    		<div class="row mb-5">
          <div class="col-md-4 text-center py-4">
          	<div class="icon">
          		<span class="icon-envelope-o"></span>
          	</div>
            <p><span>Email:</span> <a href="sales@sellmacdirect.co.uk">sales@sellmacdirect.co.uk</a></p>
          </div>
          <div class="col-md-4 text-center border-height py-4">
          	<div class="icon">
          		<span class="icon-mobile-phone"></span>
          	</div>
            <p><span>Phone:</span> <a href="tel://1234567920">02085 7598 50</a></p>
          </div>
          <div class="col-md-4 text-center py-4">
          	<div class="icon">
          		<span class="icon-map-o"></span>
          	</div>
            <p><span>Address:</span>25 Sipson Road, West Drayton, England UB7 9DQ</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<?php include 'layouts/footer.php';?>