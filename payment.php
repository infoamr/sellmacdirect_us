<?php ini_set('display_errors', 0);
include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
    <div class="condition-top-section">
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                <h1 id="progress-bar-header" class="text-center">Payment Method</h1>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                   <div id="progress-bar" class="row condition-progress-bar">
                      <div class="col-3 cp-box first">
                         <span>Your details</span>
                         <div class="tick"></div>
                      </div>
                      
                      <div class="col-3 cp-box cp-box-process cp-box-active js-active">
                         <span>Payment Method</span>
                         <div class="tick"></div>
                      </div>
                      
                      <div class="col-3 cp-box">
                         <span>Collection Date</span>
                         <div class="tick"></div>
                      </div>
                      
                      <div class="col-3 cp-box last">
                         <span>Confirm Order</span>
                         <div class="tick"></div>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>

    <div class="condition-top-section">
       <div class="container">
          <div class="row">
             <div id="checkout-container" class="col-12">
                <form id="checkout-step-form" class="d-none"> <input type="hidden" name="checkout_step"> </form>
                <div id="checkout-content" class="your-order-box">
                   <?php
                            $name=$_POST["name"];
                            $mdlm=$_POST['mdl_name'];
                            $mdlp0=$_POST['mdl_prs0'];
                            $mdlp1=$_POST['mdl_prs1'];
                            $mdlp=$_POST['mdl_prs'];
                            $mdls=$_POST['mdl_scr'];
                            $mdna=$_POST['mdl_mod'];
                            $modn=$_POST['model_name'];
                            $ser=$_POST['serial_number'];
                            $radio=$_POST['power'];
                            $mac_condition_yes=$_POST['mac_condition_yes'];
                            $mac_condition_no=$_POST['mac_condition_no'];
                            $price=$_POST['price'];
                            $rdio=$_POST['work_adap'];
                            $Mobile_Number=$_POST["mobile"];
                            $Email=$_POST["email"];
                            
                            $mac_ram = $_POST['mac_ram'];
                            $ramoth = $_POST['ramoth'];
                            $mac_storage = $_POST['mac_storage'];
                            $storageoth = $_POST['storageoth'];
                                  
                            //echo $mac_ram,$ramoth,$mac_storage,$storageoth; exit;
                            
                            $deviceprice = $_POST['deviceprice'];
                            $thumbnailUrl = $_POST['thumbnailUrl'];
                            $model = $_POST['model'];
                            $item = $_POST['item'];
                            $pro_type = $_POST['pro_type'];

                            $modelNumber = $_POST["modelNumber"];
                            $modelIdentifier = $_POST["modelIdentifier"];
                            $standardRAM = $_POST['standardRAM'];
                            $standardHD = $_POST['standardHD'];
                    ?>
                   <p class="text-center">Select the method by which you would like to recieve your cash</p>
                   <div class="radio-box" style="padding-left:40px;" >
                      <div class="custom-control custom-radio">
                         <input type="radio"   name="Selection" id="Selection-1" class="custom-control-input paypal-btn" onclick="switchVisible()" >
                         <label class="custom-control-label" for="Selection-1">
                            <div class='box-image'>
                               <img height="50px;" src="images/paypal.png" healt="PayPal">
                            </div>
                            PayPal
                         </label>
                      </div>
                   </div>
                   <div class="radio-box" style="padding-left:40px;">
                      <div class="custom-control custom-radio">
                         <input type="radio" name="Selection" id="Selection-2" class="custom-control-input bank-btn" onclick="switchVisible1()" >
                         <label class="custom-control-label" for="Selection-2">
                            <div class='box-image'>
                               <img height="80px;" src="images/faster-payment.png" alt="Bank Transfer">
                            </div>
                            Bank Transfer
                         </label>
                      </div>
                   </div>
                   <div class="clear"></div>
                   <hr>
                   <div id="Selection-1-container" style="display:none;">
                      <p>Once we have received and checked over your items we’ll send your cash to your PayPal account. If you don’t have a paypal account, you can still receive your founds. All we need is a valid email address.</p>
                      <form action="collectdate.php" method="post" class="edit-payment-form">
                         <div class="input-box">
                            <label>Your PayPal Email Address</label>
                            <?php //echo $name, $Mobile_Number, $Email; ?>
                            <input type="hidden" name="payment_type" value="Paypal">
                            <input name="paypal_email" value="" type="email" placeholder="you@example.com" required>
                            <input type="hidden" name="mdl_name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mdl_prs0" value="<?php echo $mdlp0; ?>">
                            <input type="hidden" name="mdl_prs1" value="<?php echo $mdlp1; ?>">
                            <input type="hidden" name="mdl_prs" value="<?php echo $mdlp; ?>">
                            <input type="hidden" name="mdl_scr" value="<?php echo $mdls; ?>">
                            <input type="hidden" name="mdl_mod" value="<?php echo $mdna; ?>">
                            <input type="hidden" name="model_name" value="<?php echo $modn; ?>">
                            <input type="hidden" name="serial_number" value="<?php echo $ser; ?>">
                            <input type="hidden" name="power" value="<?php echo $radio; ?>">
                            <input type="hidden" name="mac_condition_yes" value="<?php echo $mac_condition_yes; ?>">
                            <input type="hidden" name="mac_condition_no" value="<?php echo $mac_condition_no; ?>">
                            <input type="hidden" name="price" value="<?php echo $price ?>">
                            <input type="hidden" name="work_adap" value="<?php echo $rdio; ?>">
                            
                            <input type="hidden" name="name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mobile" value="<?php echo $Mobile_Number ?>">
                            <input type="hidden" name="email" value="<?php echo $Email; ?>">
                            
                            <input type="hidden" name="mac_ram" value="<?php echo $mac_ram; ?>">
                            <input type="hidden" name="ramoth" value="<?php echo $ramoth ?>">
                            <input type="hidden" name="mac_storage" value="<?php echo $mac_storage; ?>">
                            <input type="hidden" name="storageoth" value="<?php echo $storageoth; ?>">
                            
                            <input type="hidden" name="deviceprice" value="<?php echo $deviceprice; ?>">
                            <input type="hidden" name="item" value="<?php echo $item; ?>">
                            <input type="hidden" name="model" value="<?php echo $model; ?>">
                            <input type="hidden" name="thumbnailUrl" value="<?php echo $thumbnailUrl; ?>">
                            <input type="hidden" name="pro_type" value="<?php echo $pro_type; ?>">
                            <input type="hidden" name="modelNumber" value="<?php echo $modelNumber; ?>">
                            <input type="hidden" name="modelIdentifier" value="<?php echo $modelIdentifier; ?>">
                            <input type="hidden" name="standardRAM" value="<?php echo $standardRAM; ?>">
                            <input type="hidden" name="standardHD" value="<?php echo $standardHD; ?>">
                                    
                         </div>
                         <div class="submit-box">
                            <button type="submit" class="button">continue <span>&gt;</span></button>
                         </div>
                      </form>
                   </div>
                   <div id="Selection-2-container" class="bank" style="display:none;">
                      <form action="collectdate.php" method="post" class="edit-payment-form">
                            <input type="hidden" name="payment_type" value="Bank Transfer">
                            <input type="hidden" name="mdl_name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mdl_prs0" value="<?php echo $mdlp0; ?>">
                            <input type="hidden" name="mdl_prs1" value="<?php echo $mdlp1; ?>">
                            <input type="hidden" name="mdl_prs" value="<?php echo $mdlp; ?>">
                            <input type="hidden" name="mdl_scr" value="<?php echo $mdls; ?>">
                            <input type="hidden" name="mdl_mod" value="<?php echo $mdna; ?>">
                            <input type="hidden" name="model_name" value="<?php echo $modn; ?>">
                            <input type="hidden" name="serial_number" value="<?php echo $ser; ?>">
                            <input type="hidden" name="power" value="<?php echo $radio; ?>">
                            <input type="hidden" name="mac_condition_yes" value="<?php echo $mac_condition_yes; ?>">
                            <input type="hidden" name="mac_condition_no" value="<?php echo $mac_condition_no; ?>">
                            <input type="hidden" name="price" value="<?php echo $price ?>">
                            <input type="hidden" name="work_adap" value="<?php echo $rdio; ?>">
                            
                            <input type="hidden" name="name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mobile" value="<?php echo $Mobile_Number ?>">
                            <input type="hidden" name="email" value="<?php echo $Email; ?>">
                            
                            <input type="hidden" name="mac_ram" value="<?php echo $mac_ram; ?>">
                            <input type="hidden" name="ramoth" value="<?php echo $ramoth ?>">
                            <input type="hidden" name="mac_storage" value="<?php echo $mac_storage; ?>">
                            <input type="hidden" name="storageoth" value="<?php echo $storageoth; ?>">
                            
                            <input type="hidden" name="deviceprice" value="<?php echo $deviceprice; ?>">
                            <input type="hidden" name="item" value="<?php echo $item; ?>">
                            <input type="hidden" name="model" value="<?php echo $model; ?>">
                            <input type="hidden" name="thumbnailUrl" value="<?php echo $thumbnailUrl; ?>">
                            <input type="hidden" name="pro_type" value="<?php echo $pro_type; ?>">
                            <input type="hidden" name="modelNumber" value="<?php echo $modelNumber; ?>">
                            <input type="hidden" name="modelIdentifier" value="<?php echo $modelIdentifier; ?>">
                            <input type="hidden" name="standardRAM" value="<?php echo $standardRAM; ?>">
                            <input type="hidden" name="standardHD" value="<?php echo $standardHD; ?>">
                         
                         
                         <div class="row">
                            <div class="col-md-6 col-sm-6 pr-md-2">
                             <div class="input-box"> <label>Account holder name</label> <input type="text" name="bank_account_holder" id="bank_holder" placeholder="enter name" required> </div>
                            </div>
                            
                            <div class="col-md-6 col-sm-6 pr-md-2">
                             <div class="input-box"> <label>Account number</label> <input type="text" name="bank_account_number" id="bank_acc" placeholder="enter number" required> </div>
                            </div>
                        </div>
                        
                         <div class="suggestion-box-inner">
                            <picture>
                               <img src="images/question.png" alt="Help" onmouseover="bigImg(this)" onmouseout="normalImg(this)">
                            </picture>
                            <div class="suggestion-box">
                               <p>Account number is 8 number long. This can be found on your bank statement, cheque book and in somme cases your debit card.</p>
                            </div>
                         </div>
                         
                         <p for="bank_st">Sort code</p>
                         <div class="row">
                            <div class="col-md-4 col-sm-4 pr-md-2">
                                <input name="bank_sort_code_1" id="bank_st1" value="" type="text" placeholder="09" required>
                            </div>
                            <div class="col-md-4 col-sm-4 pr-md-2">
                                <input name="bank_sort_code_2" id="bank_st2" value="" type="text" placeholder="09" required>
                            </div>
                            <div class="col-md-4 col-sm-4 pr-md-2">
                                <input name="bank_sort_code_3" id="bank_st3" value="" type="text" placeholder="09" required>
                            </div>
                         </div>
                         <br><br>      
                         <div class="submit-box">
                            <button type="submit" class="button">continue <span>&gt;</span></button>
                         </div>
                      </form>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

</section> 
   
<?php include 'layouts/footer.php';?>
 
<script>
 
//On click of the 'next' anchor
$('.accordion--form__next-btn').on('click touchstart', function() {

  var parentWrapper = $(this).parent().parent();
  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');
  var sectionFields = $(this).siblings().find('.required');


  //Validate the .required fields in this section
  var empty = $(this).siblings().find('.required').filter(function() {

    return this.value === "";

  });

  if (empty.length) {

    $('.accordion--form__invalid').show();

  } else {

    $('.accordion--form__invalid').hide();

    //If valid
    //On the next fieldset -> accordion wrapper, toggle the active class
    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

    //Close the others
    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

    //Add a class to the parent legend
    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

    //Remove the active class from the other legends
    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');

  }

  return false;
});

//On click of the 'prev' anchor
$('.accordion--form__prev-btn').on('click touchstart', function() {

  parentWrapper = $(this).parent().parent();
  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');

  //On the prev fieldset -> accordion wrapper, toggle the active class
  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

  //Close the others
  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

  //Add a class to the parent legend
  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

  //Remove the active class from the other legends
  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');


  return false;
});

// $(document).ready(function() {
//     	$('input[name="Selection"]').click(function() {

//           $('.togglehid').addClass('hidden');
//           // code changed here --> add the class hidden to all div's with class togglehid

//       	if($(this).attr('id') == 'Selection-1')
//         {
//         	$("#Selection-1-container").toggleClass('hidden');
//         }
//         else if($(this).attr('id') == 'Selection-2')
//         {
//         	$("#Selection-2-container").toggleClass('hidden');
//         }
        
//       });
//     });
    
       function switchVisible() {
            if (document.getElementById('Selection-1-container')) {

                if (document.getElementById('Selection-1-container').style.display == 'none') {
                    document.getElementById('Selection-1-container').style.display = 'block';
                     document.getElementById('Selection-2-container').style.display = 'none';
                  
                }
                else {
                    document.getElementById('Selection-2-container').style.display = 'none';
                }
            }
}

  function switchVisible1() {
            if (document.getElementById('Selection-2-container')) {

                if (document.getElementById('Selection-2-container').style.display == 'none') {
                    document.getElementById('Selection-2-container').style.display = 'block';
                    document.getElementById('Selection-1-container').style.display = 'none';

                  
                }
                else {
                    document.getElementById('Selection-1-container').style.display = 'none';
                }
            }
}
    function bigImg() {
  $('.suggestion-box').show();
}

function normalImg() {
     $('.suggestion-box').hide();
}
</script>
</body>
</html>