<?php include 'layouts/header.php';?>
<section class="ftco-section" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41);">
      <div class="overlay"></div>
      <div class="container mt-5">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5 fadeInUp ftco-animated">
            <span class="subheading">Our Warranty</span>
            <h2 class="mb-2">The smartest way to sell your used mac</h2>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
            <div class="media block-6 services d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center"><span><img src="images/choose11.png" style="width:90px; height: auto;"></span></div>
              <div class="media-body py-md-4">
                <h3>We Provide Extended Warranty for your MAC Book</h3>
                <p>Every Mac comes with a one-year limited warranty and up to 90 days of complimentary technical support. Extend your coverage with us for three years from the original purchase date of your Mac with the Extended Warranty Plan. You will get direct, one-stop access to technical support for your Mac & macOS<br>Three years of certified service and support coverage
                <br>Priority access to technical support
                </p>
              </div>
            </div>      
          </div>
          
          
          <div class="col-md-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
            <div class="media block-6 services d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center"><span><img src="images/choose112.png" style="width:90px; height: auto;"></span></div>
              <div class="media-body py-md-4">
                <h3>One stop for Technical Support</h3>
                <p>Mac is a truly integrated system. And only the Sell MAC Direct protection Plan gives you one-stop service and support from Apple experts, so most issues can be resolved in a single call.<br>Priority access to Apple experts by chat or phone call.<br>On-site repair for desktop computers: Request that a technician come to your work site.<br>Carry-in repair: Take your Mac to our Service center.





</p>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>
<?php include 'layouts/footer.php';?>