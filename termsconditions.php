<?php include 'layouts/header.php';?>
<section class="ftco-section pt-10" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41);">
      <div class="overlay"></div>
      <div class="container mt-5">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <span class="subheading">Terms & Conditions</span>
            <h2 class="mb-2">The smartest way to sell your used mac</h2>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-12 col-lg-12">
            <p><b>In these Terms & Conditions (“Terms”), the following will help you understand meanings used:</b><br>
            <b>“we/us/our” means sellmacdirect.co.uk</b></p>
            <p style="text-align: justify">These Terms apply whenever you send a your used mac hardware to us via the online process at the website and indicate your acceptance of these Terms. References to “ website” mean that sellmacdirect.co.uk only.</p>

            <p style="text-align: justify">When you use our website, We have explained how we use that information in our Privacy Policy which is also available from the link on the website. These Terms incorporate the Privacy Policy, the Terms of Website Use and FAQs, which together form the agreement between us.</p>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>About Us</b>
            <ol>
              <li style="text-align: justify">The used mac hardware sale and recycling service is provided by CHOOSEURFOOD LTD registered in England under company number 11651022 and whose registered office is 76 Coronation Street, Salford, England, M5 3SA. We use the trade name “sellmacdirect.co.uk”.</li>
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>How the contract is formed between you and us</b>
            <ol>
              <li style="text-align: justify">Our website is only intended for use by people who are resident in Great Britain.</li>
              <li style="text-align: justify">During the online process, you will be asked to agree to these Terms. You must read them carefully as they form the agreement between us and you and you will be bound by them. If and when you agree to them, we will then send a confirmation that we have received your sale order and have accepted it. The agreement between us is formed when we send that confirmation to you.</li>
              <li style="text-align: justify">You must own all rights, title and interests in any apple mac hardware that you send to us.</li>
              <li style="text-align: justify">Ownership of and risk in the hardware will only pass to us when we receive the hardware, in accordance with these Terms. Accordingly, you are strongly recommended to pack your hardware prior to dispatch to us to minimise the risk of damage.</li>
              <li style="text-align: justify">We may transfer, assign, charge, sub-contract or otherwise dispose of a contract or any of our rights or obligations arising under it at any time during the term of the contract.</li>
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>Consumers - By placing a sale order through our website, you warrant that:</b>
            <ol>
              <li style="text-align: justify"> You are resident in Great Britain; and</li>
              <li style="text-align: justify">You are accessing our website from that country; and</li>
              <li style="text-align: justify">You are legally capable of entering into a binding contract; and</li>
              <li style="text-align: justify">You are at least 18 years old (or if you are under 18 years of age, that you have obtained your parent’s or guardian's consent to sell your old apple mac hardware(s) to us for the sum indicated via our website).</li>
              <li style="text-align: justify">You and your parents or guardians release us of any liabilities or claims that may arise if you send the hardware to us in breach of this warranty.</li>
              <li style="text-align: justify">If you deal as a consumer any provision of this contract which is of no effect to a consumer shall not apply. Your statutory rights are not affected by this contract.</li>
              <li style="text-align: justify">For the purposes of these Terms, “consumer” means an individual who neither makes this contract in the course of a business, nor holds himself out as doing so, as defined by the Unfair Contract Terms Act 1977.</li>
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>Consumers - By placing a sale order through our website, you warrant that:</b>
            <ol>
              <li style="text-align: justify"> You are resident in Great Britain; and</li>
              <li style="text-align: justify">You are accessing our website from that country; and</li>
              <li style="text-align: justify">You are legally capable of entering into a binding contract; and</li>
              <li style="text-align: justify">You are at least 18 years old (or if you are under 18 years of age, that you have obtained your parent’s or guardian's consent to sell your old apple mac hardware(s) to us for the sum indicated via our website).</li>
              <li style="text-align: justify">You and your parents or guardians release us of any liabilities or claims that may arise if you send the hardware to us in breach of this warranty.</li>
              <li style="text-align: justify">If you deal as a consumer any provision of this contract which is of no effect to a consumer shall not apply. Your statutory rights are not affected by this contract.</li>
              <li style="text-align: justify">For the purposes of these Terms, “consumer” means an individual who neither makes this contract in the course of a business, nor holds himself out as doing so, as defined by the Unfair Contract Terms Act 1977.</li>
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>Conditions relating to the sale of your hardware to us</b>
            <p>A sale order does not come into effect and no contract will have been formed between us until we have accepted it. We reserve the right to refuse to process a sale order for any reason including, but not limited to, where:</p>
            <ol>
              <li style="text-align: justify"> We identify a valuation error on the website; or</li>
              <li style="text-align: justify">You fail to meet any criteria for eligibility which we impose from time to time; or</li>
              <li style="text-align: justify">You fail to submit all necessary and relevant details for us to complete the sales process; or</li>
              <li style="text-align: justify">You fail to send us your hardware(s); or</li>
              <li style="text-align: justify">Where your hardware(s) is damaged, is not consistent with the hardware grading policy below or does not comply with these Terms; or</li>
              <li style="text-align: justify">It is an imitation, copy or otherwise a non-genuine make or model.</li>              
            </ol>
            <p><b>Any hardware falling within the above criteria and similar conditions specified by us will be revalued accordingly and you will receive a regrade email to confirm this.</b></p>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>Hardware grading</b>
            <p>A sale order does not come into effect and no contract will have been formed between us until we have accepted it. We reserve the right to refuse to process a sale order for any reason including, but not limited to, where:</p>
            <ol>
              <li style="text-align: justify">Each computer hardware sold should match the make and model in the sale order and meet the following conditions where relevant to the product category:
                <ul style="list-style-type:circle">
                  <li>Working
                    <ul style="list-style-type:square">
                      <li>the unit turns on and off;</li>
                      <li>the unit is fully functional and works as it should;</li>
                      <li>the screen (where relevant) is working and intact;</li>
                      <li>the product battery is included;</li>
                      <li>the unit is not crushed or water-damaged; or</li>
                      <li>the unit includes all necessary peripherals such as controllers and power leads.</li>                      
                    </ul>
                  </li>
                </ul>
                <ul style="list-style-type:circle">
                  <li>Working with faults:
                    <ul style="list-style-type:square">
                      <li>badly damaged casing;</li>
                      <li>no battery or power lead;</li>
                      <li>battery cover missing or broken;</li>                      
                    </ul>
                  </li>
                </ul>
                <ul style="list-style-type:circle">
                  <li>Recycle only:
                    <ul style="list-style-type:square">
                      <li>physically broken or beyond economical repair;</li>
                      <li>broken/cracked/snapped hinges;</li>
                      <li>water-damaged;</li>
                      <li>the unit does not power up;</li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li style="text-align: justify">By submitting a sale order through our website you warrant that the hardware(s) comply with these Terms. The list is not exhaustive. When we inspect or test your hardware(s), we will not pay as much as the original indicative value if we find that your hardware(s) does not comply with all these conditions.</li>                       
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>Damaged hardwares/incorrect grading</b>
            <ol>
              <li style="text-align: justify">Where possible, we offer value for a damaged hardware(s) but shall not be obliged to do so nor purchase any damaged hardware(s). We value all hardware(s) in the state that we receive them in and will not be held responsible for any damage in transit. Damaged hardware(s) will be traded using the same process as for all other hardware(s). If we determine your hardware(s) is damaged or not of the grade that you initially specified when submitting your sale order, we may, at our discretion, pay for the damaged hardware(s) but the value will be less than indicated on our website during point of sale order. Accordingly, if a hardware which we receive is damaged, then we will value the hardware to take account of the damage (in accordance with our grading policy) and send a revised value by email. You can choose to continue the sale for the revised value or reject it. If so, we will return the hardware to you free of charge, the sale will not progress and our agreement will terminate. To accept or reject a revised value, either proceed or reject the revised value email. If you do not respond within 3 (three) working days, starting on the day on which we e-mail the revised value, we will automatically process your sale after that time, using the new price. In some cases, values will be zero if hardware(s) are beyond economical repair. A revised value will only be given once the hardware(s) has been tested or inspected.</li>
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>Value and payment</b>
            <ol>
              <li style="text-align: justify">Hardware values on our website are indicative only and we are not obliged to pay the initially quoted value if, when we receive your hardware, we determine that it has a lower value if it fails to meet these Terms. Values are valid for 14 days from the date on which you place a sales order except that values may vary from those initially given on our website, as provided in these Terms. We may extend the period for which hardware values are valid for but shall not under any circumstances be obliged to do so.</li>
              <li style="text-align: justify">Under no circumstances will payment be dispatched before we receive your hardware. Payment is in accordance with the method you select in the sale order process. The possible payment methods are Faster Payment or by Paypal.</li>
              <li style="text-align: justify">Payment processing and timings depend on third parties which we do not control. For payment by retail vouchers or cheque, this involves a postal service and for BACS payments, a bank. We will not be liable for delay in your receipt of payment as a result of third party action or inaction or once we have issued instructions for payment. We aim to issue instructions for payment within 24 hours of the day on which your hardware(s) is tested and confirmed to meet our Terms where valuations do not change; or within 24 hours of the expiry of the three day revalue period referred to above where a hardware value does change. Timescales are indicative only and we do not guarantee to meet them. We are advised by Royal Mail that customers should allow 3-5 working days for cheques or retail vouchers to arrive and by the banks that customers should allow up to 3 days to receive BACS payment.</li>
              <li style="text-align: justify">The criteria used to value hardwares are based on numerous factors including changes in market value. We may change the way in which we value hardwares at any time and without notice. hardware values may change from day to day which means that if you check the value of your hardware on any given day but do not place a sale order, you may be given a different value for the same hardware at a later time. This does not affect any other provisions in these Terms. Any special offers on hardware values are subject to particular terms which we may impose and may be varied or withdrawn at any time and without notice. Values are shown and payments are made in pounds sterling.</li>
              <li style="text-align: justify">Payments are also subject to validation and security checks which we or third parties may stipulate from time to time. Payments are non-transferable and will be made to the payee named as instructed in the sale order. If you would like someone else to receive payment, you must ensure you have all the correct details and authorisation. Payments will be sent in the case of postal payments to the address you stipulate when you set up your account. Payments sent in the post will be sent via second class Royal Mail post (3-5 days delivery time). Payments are made using the details which you provide. You must ensure that you provide all correct, accurate details, including without limit, payment and account details. We will not be liable if you fail to receive a payment or suffer a loss (including if a payment is sent to an incorrect account) as a result of your failure to input all payment and other details correctly, completely and accurately. In the case that a cheque duplicate needs to be raised due to incorrect details being input at the point of sale, you will be liable to cover all administrative fees issued for a duplicate cheque. The total value will be subtracted from any payments owing. If the administration fee is greater than the fee owed, no cheque payment will be sent as a result.</li>
            </ol>

            <span class="icon-long-arrow-right mr-2" style="color:#fff"></span>
            <b>General</b>
            <ol>
              <li style="text-align: justify">If any court or competent authority decides that any of the provisions of these Terms are invalid, unlawful or unenforceable to any extent, the provision will, to that extent only, be severed from the remaining terms, which will continue to be valid to the fullest extent permitted by law.</li>
              <li style="text-align: justify">If we fail, at any time while these Terms are in force, to insist that you perform any of your obligations under these Terms, that will not mean that we have we have waived such rights or remedies and will not mean that you do not have to comply with those obligations. If we do waive a default by you that will not mean that we will automatically waive any subsequent default by you. No waiver by us of any of these Terms shall be effective unless we expressly say that it is a waiver and we tell you so in writing.</li>
              <li style="text-align: justify">A person who is not a party to these Terms shall not have any rights under or in connection with them under the Contracts (Rights of Third Parties) Act 1999.</li>
              <li style="text-align: justify">We may also provide links on our site to the websites of other companies, whether affiliated with us or not. We cannot give any undertaking that products you purchase from third party sellers through our site or from companies to whose website we have provided a link on our website will be of satisfactory quality and any such warranties are DISCLAIMED by us absolutely. This DISCLAIMER does not affect your statutory rights against that third party seller.</li>
              <li style="text-align: justify">These Terms and any document expressly referred to in them represent the entire agreement between us in relation to the subject matter of any contract and supersede any prior agreement, understanding or arrangement between us, whether oral or in writing.</li>
              <li style="text-align: justify">These Terms shall be governed by English law and we both agree to the non-exclusive jurisdiction of the English Courts.</li>
            </ol>

            












            
          	    





          	    	
          </div>


          
        </div>
      </div>
    </section>














<?php include 'layouts/footer.php';?>