<?php include 'layouts/header.php';?>
<section class="ftco-section" style="background-image: linear-gradient(to right bottom, #9dc88d, #a8c77e, #b6c46e, #c6c060, #d8bb54, #c6b24c, #b5a945, #a4a03f, #728e42, #487947, #286247, #164a41);">
      <div class="overlay"></div>
      <div class="container mt-5">
        <div class="row justify-content-start mb-5">
          <div class="col-md-12 text-center heading-section heading-section-white ftco-animate fadeInUp ftco-animated">
            <span class="subheading">Recycle</span>
            <h2 class="mb-3">There are lots of reasons to, but these are the main ones…</h2>
          </div>
        </div>
        <div class="row">
          <div class="work-flow col-md-12 col-lg-12">
            <div class="row">
              <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                <div class="media block-6 services services-2">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>01</span></div>
                    <h3>Maximun Payout</h3>
                    <p>We provide you the best value for your used gadgets within a week.</p>
                  </div>
                </div>      
              </div>
              <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                <div class="media block-6 services services-2">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>02</span></div>
                    <h3>We arrange a free collection</h3>
                    <p>Book a convenient, free, collection any day of the week, from your home or place of work.</p>
                  </div>
                </div>      
              </div>
              <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                <div class="media block-6 services services-2">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>03</span></div>
                    <h3>We fully wipe your data</h3>
                    <p>Full data destruction. All data on the item is parmanently removed (overwritten)</p>
                  </div>
                </div>      
              </div>
              <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                <div class="media block-6 services services-2">
                  <div class="media-body py-md-4 text-center">
                    <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>04</span></div>
                    <h3>We test your item & get you paid quickly</h3>
                    <p>You get paid the quoted amount, or your item is returned free of charge.</p>
                  </div>
                </div>      
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include 'layouts/footer.php';?>