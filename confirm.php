<style>

h4 {
    font-weight: 600;
    color: #415093;
    line-height: 24px;
    font-size: 21px;
    margin: 0 0 20px;
}

.radio-box label {
    margin: 0 0 0 25px;
    display: inline-block;
    vertical-align: top;
    line-height: 17px;
    font-size: 15px;
    color: #222;
    font-weight: normal;
}

.bg-grey {
    background: #f8f8ff;
}

.offer-step.condition-inner {
    padding: 50px 100px;
}

.condition-inner {
    max-width: 700px;
    margin: 0px auto 70px;
    background: #fff;
    box-shadow: 0 -6px 57px 0 rgba(65,80,147,.24);
    border-radius: 12px;
    padding: 50px;
}

 .offer-step .rb-box {
    max-width: 280px;
    margin: 0 auto 10px;
}

.rb-box {
    margin-bottom: 25px;
}

.price-accept {
    text-align: center;
    margin: 0 0 60px;
}

.gnh-box {
    margin: 0 0 30px;
}

.text-center {
    text-align: center!important;
}

.offer-p {
    margin: 0;
    opacity: .5;
}

.form-radio:checked {
    background-color: #11c47e !important;
    border: none;
}

.offer-step.condition-inner {
    padding: 50px 100px;
}

.your-order-box {
    max-width: 700px;
    background: #fff;
    padding: 70px;
    border-radius: 12px;
    margin: 90px auto 60px;
    box-shadow: 0 -6px 57px 0 rgba(65,80,147,.24);
}

.your-order-box h3 {
    text-align: center;
    margin-bottom: 30px;
    font-weight: bold;
    color: #363639;
    line-height: 28px;
    font-size: 24px;
    margin: 0 0 15px;
}

input[type="text"], input[type="search"], input[type="email"], input[type="password"], textarea, input[type="date"] {

    width: 100%;
    border: 2px solid #415093;
    border-radius: 20px;
    height: 38px;
    line-height: 34px;
    padding: 0 15px;
    margin: 0;
    color: #415093;
    font-size: 12px;
    background: #fff;
    outline: none;
    font-size: 14px;
}

.your-order-box .input-box {
    padding: 6px;
}

.input-box.left {
    float: left;
    width: 48.5%;
}

.input-box {
    margin: 0 0 30px;
}

.your-order-box .col-md-4 .green-link {
    white-space: nowrap;
    margin-top: 32px;
    margin-left: -10px;
}

.your-order-box .col-md-4 .button {
    white-space: nowrap;
    margin-top: 25px;
    height: 40px;
    line-height: 40px;
    padding: 0 30px;
}

.your-order-box .red-box {
    margin-bottom: 40px;
}

.red-box {
    background: #fdf3ec;
    border: 1px solid #e5dbd3;
    border-radius: 8px;
    margin: 0 0 25px;
    padding: 15px 15px 15px 60px;
    position: relative;
}

.red-box span {
    position: absolute;
    background: #f87012;
    line-height: 24px;
    height: 24px;
    width: 24px;
    text-align: center;
    color: #fff;
    font-weight: bold;
    font-style: italic;
    border-radius: 24px;
    left: 15px;
    top: 12px;
}

button[type="submit"], .button {
    box-shadow: 0 5px 29px 0 #84ffcf;
    background: #11c47e;
    border: none;
    color: #fff;
    border-radius: 30px;
    line-height: 60px;
    height: 60px;
    padding: 0 30px 0 70px;
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: 1.52px;
    cursor: pointer;
    display: inline-block;
    outline: none;
}

button[type="submit"]:hover, .button:hover {
    box-shadow: 0 5px 29px 0 #415093;
    background: #415093;
    outline: none;
}

button[type="submit"] span, .button span {
    padding-left: 30px;
}

.form-radio:checked::before {
    position: absolute;
    left: 6px;
    top: 0;
    content: '\02143';
    transform: rotate(40deg);
    color: #fff;
    font-weight: bold;
    font-size: 10px;
}

.green-link {
    display: inline-block;
    vertical-align: middle;
    margin: 0px;
    font-size: 16px;
    color: #11c47e;
    text-decoration: underline;
    font-weight: 600;
    cursor: pointer;
    background: none;
    padding: 0;
}

.green-link img, .white-link img {
    margin: 0 0 0 7px;
    width: 15px;
}

button.green-link, button.white-link {
    border: none;
}

.gnh-box {
    margin: 0 0 30px;
}

.offer-step .gnh-box .radio-box {
    margin: 0 15px 0 0;
}

.radio-box {
    margin: 0 35px 15px 0;
    line-height: 100%;
    display: inline-block;
    position: relative;
}

.form-radio {
     -webkit-appearance: none; 
    position: absolute;
    background: #fff;
    top: 0px;
    left: 0;
    height: 17px;
    width: 17px;
    border-radius: 50px;
    cursor: pointer;
    outline: none;
    border: 1px solid #dadada;
    line-height: 17px;
}

.price {
    font-size: 56px;
    line-height: 56px;
    color: #415093;
    font-weight: 600;
    margin: 0 0 30px;
}

.mod_btn
{
    box-shadow: 0 5px 29px 0 #84ffcf;
    background: #11c47e;
    padding: 20px;
    border-radius: 30px;
    color: #fff;
    border: none;
}

.mod_btn:hover
{
    box-shadow: 0 5px 29px 0 #415093;
    background: #415093;
    color:#fff;
    text-decoration: none !important;
}

</style>

<?php ini_set('display_errors', 0);
include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
    
    <div class="condition-top-section">
        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                <h1 id="progress-bar-header" class="text-center">Confirm Order</h1>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                   <div id="progress-bar" class="row condition-progress-bar">
                      <div class="col-3 cp-box first">
                         <span>Your details</span>
                         <div class="tick"></div>
                      </div>
                      
                      <div class="col-3 cp-box">
                         <span>Payment Method</span>
                         <div class="tick"></div>
                      </div>
                      
                      <div class="col-3 cp-box">
                         <span>Collection Date</span>
                         <div class="tick"></div>
                      </div>
                      
                      <div class="col-3 cp-box cp-box-process cp-box-active js-active last">
                         <span>Confirm Order</span>
                         <div class="tick"></div>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>

<div class="condition-top-section">

   <div class="container">

        <div class="row">

            <div id="checkout-container" class="col-lg-12">

            <form id="checkout-step-form" class="d-none"> <input type="hidden" name="checkout_step">
            </form>

            <div id="checkout-content" class="your-order-box">

               <h3>Confirm your order</h3>

                    <?php
                        $name=$_POST["name"];
                        $Mobile_Number=$_POST["mobile"];
                        $Email=$_POST["email"];
                        $colldatetime=$_POST['colldatetime'];
                        $address1=$_POST['address1'];
                        $address2=$_POST["address2"];
                        $city=$_POST["city"];
                        $postcode=$_POST["postcode"];
                        
                        $payment_type=$_POST["payment_type"];
                        $paypal_email=$_POST["paypal_email"];
                        $bank_account_holder=$_POST["bank_account_holder"];
                        $bank_account_number=$_POST["bank_account_number"];
                        $bank_sort_code_1=$_POST["bank_sort_code_1"];
                        $bank_sort_code_2=$_POST["bank_sort_code_2"];
                        $bank_sort_code_3=$_POST["bank_sort_code_3"];

                        $mdlm=$_POST['mdl_name'];
                        $mdlp0=$_POST['mdl_prs0'];
                        $mdlp1=$_POST['mdl_prs1'];
                        $mdlp=$_POST['mdl_prs'];
                        $mdls=$_POST['mdl_scr'];
                        $mdna=$_POST['mdl_mod'];
                        $modn=$_POST['model_name'];
                        $ser=$_POST['serial_number'];
                        $radio=$_POST['power'];
                        $mac_condition_yes=$_POST['mac_condition_yes'];
                        $mac_condition_no=$_POST['mac_condition_no'];
                        $rdio=$_POST['work_adap'];
                        $price=$_POST['price'];

                        $sellorder_id = rand(100,5000).rand(5000,100000);
                        $shortcode = $bank_sort_code_1.$bank_sort_code_2.$bank_sort_code_3;
                        
                        $mac_ram = $_POST['mac_ram'];
                        $ramoth = $_POST['ramoth'];
                        $mac_storage = $_POST['mac_storage'];
                        $storageoth = $_POST['storageoth'];
                        
                        $deviceprice = $_POST['deviceprice'];
                        $thumbnailUrl = $_POST['thumbnailUrl'];
                        $model = $_POST['model'];
                        $item = $_POST['item'];
                        $pro_type = $_POST['pro_type'];
                        $modelNumber = $_POST['modelNumber'];
                    ?>
                    <?php
                    include 'layouts/connection.php';

                    $sql = "UPDATE customer SET 
                           address1 = '$address1',
                           address2 = '$address1',
                           city = '$city',
                           postcode = '$postcode',
                           colldatetime = '$colldatetime'
                            WHERE name='$name'";
                            
                    if (mysqli_query($conn, $sql)) {
                     // echo "New record created successfully !";
                    } else {
                           echo "Error: " . $sql . "" . mysqli_error($conn);
                        }
                    ?>
                
               <form action="thankyou.php" class="account-form" method="post">

                  <input type="hidden" name="country" value="United Kingdom">
                  <input type="hidden" name="sellorder_id" value="<?php echo $sellorder_id; ?>">
                  <input type="hidden" name="model_name" value="<?php echo $modn; ?>">
                  <input type="hidden" name="serial_number" value="<?php echo $ser; ?>">
                  <input type="hidden" name="power" value="<?php echo $radio; ?>">
                  <input type="hidden" name="mac_condition_yes" value="<?php echo $mac_condition_yes; ?>">
                  <input type="hidden" name="mac_condition_no" value="<?php echo $mac_condition_no; ?>">
                  <input type="hidden" name="price" value="<?php echo $price;?>">
                  <input type="hidden" name="work_adap" value="<?php echo $rdio; ?>">
                  <input type="hidden" name="email" value="<?php echo $Email; ?>">
                  <input type="hidden" name="mobile" value="<?php echo $Mobile_Number ?>">
                  <input type="hidden" name="pay_method" value="<?php echo $payment_type; ?>">
                  <input type="hidden" name="paypal_email" value="<?php echo $paypal_email; ?>">
                  <input type="hidden" name="account_holdername" value="<?php echo $bank_account_holder; ?>">
                  <input type="hidden" name="account_number" value="<?php echo $bank_account_number; ?>">
                  <input type="hidden" name="shortcode" value="<?php echo $shortcode; ?>">
                  
                  <input type="hidden" name="name" value="<?php echo $name; ?>">
                  <input type="hidden" name="address1" value="<?php echo $address1; ?>">
                  <input type="hidden" name="address2" value="<?php echo $address2; ?>">
                  <input type="hidden" name="city" value="<?php echo $city; ?>">
                  <input type="hidden" name="postcode" value="<?php echo $postcode; ?>">
                  <input type="hidden" name="colldatetime" value="<?php echo $colldatetime; ?>"> 
                  
                  <input type="hidden" name="mac_ram" value="<?php echo $mac_ram; ?>">
                  <input type="hidden" name="ramoth" value="<?php echo $ramoth; ?>">
                  <input type="hidden" name="mac_storage" value="<?php echo $mac_storage; ?>">
                  <input type="hidden" name="storageoth" value="<?php echo $storageoth; ?>">
                    
                    <input type="hidden" name="deviceprice" value="<?php echo $deviceprice; ?>">
                    <input type="hidden" name="item" value="<?php echo $item; ?>">
                    <input type="hidden" name="model" value="<?php echo $model; ?>">
                    <input type="hidden" name="thumbnailUrl" value="<?php echo $thumbnailUrl; ?>">
                    <input type="hidden" name="pro_type" value="<?php echo $pro_type; ?>">
                    <input type="hidden" name="modelNumber" value="<?php echo $modelNumber; ?>">

                  <div class="d-flex justify-content-between">

                    <div class="input-box left d-flex flex-column justify-content-between col-md-12">
                        <div class="suggestion-box-inner">
                            <table style="width:100%; font-size:16px; ">
                                <tr style="">
                                <td colspan="2" align="center" style="width:40%;"><h4><b>Be sure to double check all of your details before you confirm</b></h4></td>
                                </tr>

                                <tr style="line-height:2">
                                <td colspan="2" align="center" class="price" style="width:100%;">
                                    <h1>£<?php if($price != '') { echo $price; } else { echo $deviceprice; }?></h1></td>
                                </tr>
                                
                                <?php if($ser != '') { ?>
                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Device Serial:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;">
                                    <?php echo $ser; ?>
                                </td>
                                </tr>
                                <?php } ?>

                                <tr style="line-height:2">
                                <td style="width:50%; padding-left:18%; "><b>Processor Type:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;">
                                    <?php echo $pro_type; ?>
                                </td>
                                </tr>
                                

                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Model Number:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;">
                                    <?php echo $modelNumber; ?>
                                </td>
                                </tr>
                                
                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Standard HD:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;">
                                    <?php echo $mac_storage; ?>
                                </td>
                                </tr>

                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Standard RAM:</b></td>
                                <td align="left" style="width:50%;padding-left:0%">
                                    <?php echo $mac_ram; ?></td>
                                </tr>
                            </table>
                            <hr>
                            <div class="shipping-box mb-3">
                                <div class="radio-box">
                                    <input type="radio" class="form-radio" checked>
                                    <label>Payment Details</label>
                                </div>
                            </div>

                            <table style="width:100%; font-size:16px;">
                                
                                <tr style="line-height:2">
                                <td style="width:50%; padding-left:18%; "><b>Pay Method:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;"><?php echo $payment_type; ?></td>
                                </tr>

                                <tr style="line-height:2">
                                <td style="width:50%; padding-left:18%; "><b>Customer Name:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;"><?php echo $name; ?></td>
                                </tr>
                                
                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Email Address:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;"><?php echo $Email; ?></td>
                                </tr>
                                
                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Contact Number:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;"><?php echo $Mobile_Number; ?></td>
                                </tr>
                            </table>
                            <hr>
                            <div class="shipping-box mt-5">
                                <div class="radio-box">
                                    <input type="radio" class="form-radio" checked>
                                    <label>Hardware Pickup Details</label>
                                </div>
                            </div>
                            
                            <table style="width:100%; font-size:16px; ">
                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Collect Date:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;"><?php echo $colldatetime; ?></td>
                                </tr>
                                
                                <tr style="line-height:2">
                                <td style="width:50%;padding-left:18%;"><b>Address:</b></td>
                                <td align="left" style="width:50%;padding-left:0%;"><?php echo $address1.' '.$address2.' '.$city.' '.$postcode; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                  </div>

                  <div class="clear"></div>

                  <div class="submit-box"> 

                  <button type="submit" class="js-help-modal mod_btn" id="mod_btn" rel="help" data-max-width="800px" data-target="#js-offer-modal" data-toggle="modal" data-backdrop="static" data-keyboard="false">Continue<span>&gt;</span></button>

                  </div>

               </form>

            </div>

         </div>

</div>

   </div>

</div>
</section> 
<?php include 'layouts/footer.php'; ?>