<?php include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
  <div class="condition-top-section">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <h1 class="mb-4" style="none">It’s easy to sell your Macbook for cash!</h1>
            <p style="font-size: 18px;">In order to offer an accurate price we need to know the age and specification of your macbook</p>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
               <div id="progress-bar" class="row condition-progress-bar">
                  <div class="col-3 cp-box cp-box-process cp-box-active js-active first">
                     <span style="font-size: 18px;">Select Type</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Select Generation</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Condition &amp; Functionality</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box last">
                     <span style="font-size: 18px;">Confirm offer</span>
                     <div class="tick"></div>
                  </div>
               </div>
            </div>
        </div>
    </div>
  </div>

<!-- Type Section Start -->

<section class="flex-grow-1 d-flex flex-column">
 <div class="buttons condition-box">
    <div class="container">
     <div class="row">
       <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
          <h4 class="mb-4">Select Your Mac Pro Type</h4>
          <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
             <span>How do I find this</span>
             <picture>
                <img src="images/question.png" alt="Help"> 
             </picture>
          </button>
          <br><br>
       </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_ver where mac_id='pro'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo "<div class='container'>";
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
     echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
     if($row['id']=='6')
     {
        echo "<a class='show box type_2' target='2' data-t_id='{$row['id']}'>";
        echo "<div class='box-image'>";
        echo "<img src='images/macbook.png'>";
        echo "</div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
     }
     elseif($row['id']=='7')
     {
        echo " <a  class='show box type_3' target='3' data-t_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/macbook.png'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
     }
     elseif($row['id']=='8')
     {
        echo " <a  class='show box type_4' target='4' data-t_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/macbook.png'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
     }
     else
     {
        echo " <a  class='show box type' target='1' data-t_id='{$row['id']}'>";
        echo " <div class='box-image'>";
        echo " <img src='images/macbook.png'>";
        echo" </div>";
        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
    }
    }
    echo "</div>";
    echo "</div>";
    
} ?>

</div>
</div>

<!-- Type Section End -->



<!-- /////////////////////////  START Macbook Pro Optical Drive, Black Keyboard (2008 - 2012) ////////////////////////// -->

<!-- Processor Section Start -->

<div class="container">
<div id="div1" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Silver Keyboard Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
               <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>

<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_prs WHERE id='10' ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==10)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <a class='show2 box fixed_1_1' target='11' data-p_id='{$row['id']}' >";
            echo " <div class='box-image'>";
            echo " <img src='images/intelcore2.png'  alt='Macbook Intel Core i5'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p>";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<!-- Processor Section End -->

<!-- Screen Section Start -->

<div class="container">
<div id="div11" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
            echo " <a class='show2 box s_11_1' target='111' data-s_id='{$row['id']}'>";
            echo " <div class='box-image' >";
            echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".$row['size']."</p> ";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==3)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
            echo " <a class='show2 box s_11_2' target='112' data-s_id='{$row['id']}'>";
            echo " <div class='box-image' >";
            echo " <img src='images/macbook-pro-15.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".$row['size']."</p> ";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==5)
        {
     
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
            echo " <a class='show2 box s_11_3' target='113' data-s_id='{$row['id']}'>";
            echo " <div class='box-image' >";
            echo " <img src='images/macbook-pro-17.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".$row['size']."</p> ";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>

</div>
</div>   
</div>

<!-- Screen Section End -->

<!-- Model Section Start -->

<div class="container">
<div id="div111" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==19)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my1Form'>";
            echo " <div class='box-image' onclick='my1Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_1'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_1_core2'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_11_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>

</div>
</div>

<div class="container">
<div id="div112" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==20)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my2Form'>";
            echo " <div class='box-image' onclick='my2Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_1'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_1_core2'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_11_1_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div113" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==21)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my3Form'>";
            echo " <div class='box-image' onclick='my3Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_1'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_1_core2'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_11_1_17'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<!-- Model Section End -->
<!-- /////////////////////// Macbook Pro Optical Drive, Black Keyboard (2008 - 2012) ////////////////////////// -->










<!-- ////////////////////// Start MacBook Pro Retina, No Name on Bezel (2012 - 2015)  ///////////////////////// -->

<!-- Processor Section Start -->

<div class="container">
<div id="div2" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Silver Keyboard Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
               <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>

<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_prs";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==8)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <a class='show2 box fixed_2_1' target='21' data-p_id='{$row['id']}' >";
            echo " <div class='box-image'>";
            echo " <img src='images/intelcorei7.png'  alt='Macbook Intel Core i5'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p>";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==9)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <a class='show2 box fixed_2_2' target='22' data-p_id='{$row['id']}' >";
            echo " <div class='box-image'>";
            echo " <img src='images/intelcorei5.png'  alt='Macbook Intel Core i5'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p>";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<!-- Processor Section End -->

<!-- Screen Section Start -->

<div class="container">
<div id="div21" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
            echo " <a class='show2 box s_21_1' target='211' data-s_id='{$row['id']}'>";
            echo " <div class='box-image' >";
            echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".$row['size']."</p> ";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==3)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
            echo " <a class='show2 box s_21_2' target='212' data-s_id='{$row['id']}'>";
            echo " <div class='box-image' >";
            echo " <img src='images/macbook-pro-15.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".$row['size']."</p> ";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>

<div class="container">
<div id="div22" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
            echo " <a class='show2 box s_21_1' target='221' data-s_id='{$row['id']}'>";
            echo " <div class='box-image' >";
            echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".$row['size']."</p> ";
            echo "</p>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>



<!-- Screen Section End -->

<!-- Model Section Start -->

<div class="container">
<div id="div211" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==19)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my4Form'>";
            echo " <div class='box-image' onclick='my4Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==1)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my5Form'>";
            echo " <div class='box-image' onclick='my5Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        } 
        if($row['id']==2)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my6Form'>";
            echo " <div class='box-image' onclick='my6Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div212" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==19)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my4Form'>";
            echo " <div class='box-image' onclick='my4Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_2_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==1)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my5Form'>";
            echo " <div class='box-image' onclick='my5Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_2_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        } 
        if($row['id']==2)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my6Form'>";
            echo " <div class='box-image' onclick='my6Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_2_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div221" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
 echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==22)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my7Form'>";
            echo " <div class='box-image' onclick='my7Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==23)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my8Form'>";
            echo " <div class='box-image' onclick='my8Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_2'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_2_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_21_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<!-- Model Section End -->
<!-- ////////////////////////////// END Macbook Pro Optical Drive, Black Keyboard (2008 - 2012) ///////////////////////////// -->







<!-- ////////////////////////////// Start MacBook Pro Retina, No Name on Bezel (2012 - 2015) ///////////////////////////// -->

<!-- Processor Section Start -->

<div class="container">
<div id="div3" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Silver Keyboard Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
               <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>

<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_prs";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==8)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed_3_1' target='31' data-p_id='{$row['id']}' >";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei7.png'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p>".trim($row['name'])."</p>";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==9)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed_3_2' target='32' data-p_id='{$row['id']}' >";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei5.png'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p>".trim($row['name'])."</p>";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==11)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed_3_3' target='33' data-p_id='{$row['id']}' >";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei9.png'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p>".trim($row['name'])."</p>";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
    }
    echo "</div>";
}
?>

</div>
</div>

<!-- Processor Section End -->

<!-- Screen Section Start -->

<div class="container">
<div id="div31" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_31_1' target='311' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==3)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_31_2' target='312' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-15.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==4)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_31_3' target='313' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-16.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>

<div class="container">
<div id="div32" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_31_1' target='321' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>

<div id="div33" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==3)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_31_2' target='331' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-15.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==4)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_31_3' target='332' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-16.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>

<!-- Screen Section End -->
<!-- Model Section Start -->

<div class="container">
<div id="div311" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==5)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my9Form'>";
            echo " <div class='box-image' onclick='my9Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==3)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my10Form'>";
            echo " <div class='box-image' onclick='my10Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        } 
        if($row['id']==9)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my11Form'>";
            echo " <div class='box-image' onclick='my11Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==10)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my12Form'>";
            echo " <div class='box-image' onclick='my12Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div312" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==4)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my13Form'>";
            echo " <div class='box-image' onclick='my13Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_2_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==18)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my14Form'>";
            echo " <div class='box-image' onclick='my14Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_2_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div313" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==24)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my15Form'>";
            echo " <div class='box-image' onclick='my15Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_3_16'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div321" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==5)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my16Form'>";
            echo " <div class='box-image' onclick='my16Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==3)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my17Form'>";
            echo " <div class='box-image' onclick='my17Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        } 
        if($row['id']==9)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my18Form'>";
            echo " <div class='box-image' onclick='my18Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==10)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my19Form'>";
            echo " <div class='box-image' onclick='my19Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div331" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==18)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my20Form'>";
            echo " <div class='box-image' onclick='my20Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i9'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_2_15'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div332" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==24)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my21Form'>";
            echo " <div class='box-image' onclick='my21Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_3'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_3_i9'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_31_3_16'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>



<!-- Model Section End -->
<!-- ////////////////////////////// End MacBook Pro Retina, No Name on Bezel (2012 - 2015) ///////////////////////////// -->







<!-- ////////////////////////////// Start Macbook Pro Silicon (2020 - Current) ///////////////////////////// -->
<!-- Processor Section Start -->

<div class="container">
<div id="div4" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your Macbook Pro Silver Keyboard Processor</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
               <span>How do I find this</span>
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>

<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_prs";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==8)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed_4_1' target='41' data-p_id='{$row['id']}' >";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei7.png'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p>".trim($row['name'])."</p>";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==9)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed_4_2' target='42' data-p_id='{$row['id']}' >";
        echo " <div class='box-image'>";
        echo " <img src='images/intelcorei5.png'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p>".trim($row['name'])."</p>";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        if($row['id']==16)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
        echo " <a class='show2 box fixed_4_3' target='43' data-p_id='{$row['id']}' >";
        echo " <div class='box-image'>";
        echo " <img src='images/macm1.jpg'  alt='Macbook Intel Core i5'>";
        echo" </div>";
        echo "<p>".trim($row['name'])."</p>";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
    }
    echo "</div>";
}
?>

</div>
</div>

<!-- Processor Section End -->

<!-- Screen Section Start -->

<div class="container">
<div id="div41" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_41_1' target='411' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        
        
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>

<div class="container">
<div id="div42" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_41_1' target='421' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        
        
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>

<div class="container">
<div id="div43" class="targetDiv">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Screen Size</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span> 
               <picture>
                  <img src="images/question.png" alt="Help"> 
               </picture>
            </button>
            <br><br>
        </div>
    </div>
<div class="container">
 
<?php
include 'layouts/connection.php';

$sql = "SELECT * from macbook_size";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='row justify-content-center'>";
    while($row = $result->fetch_assoc()) {
        if($row['id']==2)
        {
        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4'>";
        echo " <a class='show2 box s_41_1' target='431' data-s_id='{$row['id']}'>";
        echo " <div class='box-image' >";
        echo " <img src='images/macbook-pro-13.png'  alt='model number'>";
        echo" </div>";
        echo "<p>".$row['size']."</p> ";
        echo "</p>";
        echo "</a>";
        echo "</div>";
        }
        
        
    }
    echo "</div>";
}
?>
</div>
</div>   
</div>
<!-- Screen Section End -->

<!-- Model Section Start -->

<div class="container">
<div id="div411" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==7)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my22Form'>";
            echo " <div class='box-image' onclick='my22Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_4'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_4_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_41_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==6)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my23Form'>";
            echo " <div class='box-image' onclick='my23Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_4'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_4_i7'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_41_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div421" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==7)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my24Form'>";
            echo " <div class='box-image' onclick='my24Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_4'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_4_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_41_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
        if($row['id']==6)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my25Form'>";
            echo " <div class='box-image' onclick='my25Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_4'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_4_i5'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_41_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>

<div class="container">
<div id="div431" class="targetDivs">
    <div class="row d-flex">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
            <h4>Select Your MacBook Pro TouchBar Model Number</h4>
            <button type="button" class="green-link js-help-modal" data-max-width="700px" data-toggle="modal" data-target="#js-help-modal">
            <span>How do I find this</span>
            <picture>
              <img src="images/question.png" alt="Help"> 
            </picture>
            </button>
            <br><br>
        </div>
    </div>
<?php
include 'layouts/connection.php';

$sql = "SELECT * from mac_model";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
    // output data of each row
    echo " <div class='row justify-content-center'>";
        while($row = $result->fetch_assoc()) {
        if($row['id']==25)
        {
            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4' >";
            echo " <form  action='intelprocess.php' class='box' id='my26Form'>";
            echo " <div class='box-image' onclick='my26Function()'>";
            echo " <img src='images/image10.png'  alt='model number'>";
            echo" </div>";
            echo "<p>".trim($row['name'])."</p> ";
            echo "<input type='hidden' name='mac' class='mac_v_pro_4'/>";
            echo  "<input type='hidden' name='mac_os' class='mac_p_pro_4_m1'/>";
            echo  "<input type='hidden' name='macscr' class='mac_s_41_1_13'/>";
            echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
            echo "</p>";
            echo"</form>";
            echo "</a>";
            echo "</div>";
        }
    }
    echo "</div>";
}
?>
</div>
</div>
<!-- Model Section End -->

<!-- ////////////////////////////// End Macbook Pro Silicon (2020 - Current) ///////////////////////////// -->
    <!-- Find Serial Number Pop Up starts -->
    <div class="modal fade" id="js-help-modal" role="dialog" tabindex="-1" aria-hidd="true">
        <div class="modal-dialog">
           <!-- Modal content-->
            <div class="modal-content" style="margin-top:120px;">
                <div class="modal-body position-relative" style="overflow-y: auto;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                <div class="container-fluid">
                   <div class="row mx-0">
                      <div class="mb-5">
                         <h1 class="mb-4">How do I find Serial Number - MACBOOK</h1>
                         <p>You can find your devices serial number by one of the following methods.</p>
                         <h4 class="mt-4">If your device powers on:</h4>
                         <p>1. Click the Apple icon at the top left of the screen.</p>
                         <img src="images/how_to_find.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                         <p>2. Click “About This Mac”</p>
                         <img src="images/convertto.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                         <p>3. Your serial number will be stated within the details window. You can copy the serial by highlighting the text and pressing <kbd><kbd>Cmd</kbd> + <kbd>C</kbd></kbd>.</p>
                         <img src="images/xserial.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I find The Serial Number"> 
                         <h4 class="mt-4">If your device does not power on:</h4>
                         <p>1. On the underside of your mac there will be some text below the device type, check for the text “Serial”, the following numbers will be the device serial number.</p>
                         <img src="images/macbookserial.png" class="img-fluid rounded shadow-sm d-block mx-auto" alt="How Do I find The Serial Number"> 
                      </div>
                   </div>
                </div>
                </div>
            </div>
            <!-- Modal content ends-->  
        </div>
    </div>
    <!-- Find Serial Number Pop Up ends -->
</section> 

<?php include 'layouts/footer.php';?>

<script>
 
//On click of the 'next' anchor
$('.accordion--form__next-btn').on('click touchstart', function() {

  var parentWrapper = $(this).parent().parent();
  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');
  var sectionFields = $(this).siblings().find('.required');


  //Validate the .required fields in this section
  var empty = $(this).siblings().find('.required').filter(function() {

    return this.value === "";

  });

  if (empty.length) {

    $('.accordion--form__invalid').show();

  } else {

    $('.accordion--form__invalid').hide();

    //If valid
    //On the next fieldset -> accordion wrapper, toggle the active class
    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

    //Close the others
    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

    //Add a class to the parent legend
    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

    //Remove the active class from the other legends
    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');

  }

  return false;
});

//On click of the 'prev' anchor
$('.accordion--form__prev-btn').on('click touchstart', function() {

  parentWrapper = $(this).parent().parent();
  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');

  //On the prev fieldset -> accordion wrapper, toggle the active class
  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

  //Close the others
  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

  //Add a class to the parent legend
  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

  //Remove the active class from the other legends
  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');


  return false;
});


function sendContact() {
  var valid;  
  valid = validateContact();
  if(valid) {
    jQuery.ajax({
    url: "contact_mail.php",
    data:'userName='+$("#userName").val()+'&userEmail='+$("#userEmail").val()+'&subject='+$("#userName","#subject","#userEmail").val()+'&message='+$("#content").val()+'&content='+$('#html-content-holder').html(),
    type: "POST",
    success:function(data){
    $("#mail-status").html(data);
    },
    error:function (){}
    });
  }
}



/* Start 10-04 */

function my1Function() {
  document.getElementById("my1Form").submit();
}

function my2Function() {
  document.getElementById("my2Form").submit();
}

function my3Function() {
  document.getElementById("my3Form").submit();
}

function my4Function() {
  document.getElementById("my4Form").submit();
}

function my5Function() {
  document.getElementById("my5Form").submit();
}

function my6Function() {
  document.getElementById("my6Form").submit();
}

function my7Function() {
  document.getElementById("my7Form").submit();
}

function my8Function() {
  document.getElementById("my8Form").submit();
}

function my9Function() {
  document.getElementById("my9Form").submit();
}

function my10Function() {
  document.getElementById("my10Form").submit();
}

function my11Function() {
  document.getElementById("my11Form").submit();
}

function my12Function() {
  document.getElementById("my12Form").submit();
}

function my13Function() {
  document.getElementById("my13Form").submit();
}

function my14Function() {
  document.getElementById("my14Form").submit();
}

function my15Function() {
  document.getElementById("my15Form").submit();
}

function my16Function() {
  document.getElementById("my16Form").submit();
}

function my17Function() {
  document.getElementById("my17Form").submit();
}

function my18Function() {
  document.getElementById("my18Form").submit();
}

function my19Function() {
  document.getElementById("my19Form").submit();
}

function my20Function() {
  document.getElementById("my20Form").submit();
}

function my21Function() {
  document.getElementById("my21Form").submit();
}

function my22Function() {
  document.getElementById("my22Form").submit();
}

function my23Function() {
  document.getElementById("my23Form").submit();
}

function my24Function() {
  document.getElementById("my24Form").submit();
}

function my25Function() {
  document.getElementById("my25Form").submit();
}

function my26Function() {
  document.getElementById("my26Form").submit();
}



$('.targetDiv').hide();
$('.show').click(function () {
    $('.targetDiv').hide();
    $('#div' + $(this).attr('target')).show();
});

$('.targetDivs').hide();
$('.show2').click(function () {
    $('.targetDivs').hide();
    $('#div' + $(this).attr('target')).show();
});

</script>

<!-- For Macbook Pro Optical Drive, Black Keyboard (2008 - 2012) -->

<script>
$(document).ready(function()  {
  $(".type").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_pro_1').val(t2);
  },
  
    
});
  });
}); 

</script>


<script>
$(document).ready(function()  {
  $(".fixed_1_1").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_1_core2').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_11_1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_11_1_13').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_11_2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_11_1_15').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_11_3").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_11_1_17').val(t2);
  },
  
    
});
  });
}); 

</script>

<!-- End Macbook Pro Optical Drive, Black Keyboard (2008 - 2012) -->


<!-- For MacBook Pro Retina, No Name on Bezel (2012 - 2015)      -->

<script>
$(document).ready(function()  {
  $(".type_2").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_pro_2').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_2_1").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_2_i7').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_2_2").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_2_i5').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_21_1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_21_1_13').val(t2);
  },
  
    
});
  });
}); 

</script>
<script>
$(document).ready(function()  {
  $(".s_21_2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_21_2_15').val(t2);
  },
  
    
});
  });
}); 

</script>

<!-- End MacBook Pro Retina, No Name on Bezel (2012 - 2015) -->


<!-- For MacBook Pro TouchBar / Non-TouchBar Thunderbolt 3 (2016 - 2020) -->

<script>
$(document).ready(function()  {
  $(".type_3").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_pro_3').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_3_1").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_3_i7').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_3_2").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_3_i5').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_3_3").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_3_i9').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_31_1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_31_1_13').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_31_2").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_31_2_15').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_31_3").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_31_3_16').val(t2);
  },
  
    
});
  });
});
</script>

<!-- End MacBook Pro TouchBar / Non-TouchBar Thunderbolt 3 (2016 - 2020) -->

<!-- For Macbook Pro Silicon (2020 - Current) -->

<script>
$(document).ready(function()  {
  $(".type_4").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_pro_4').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_4_1").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_4_i7').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_4_2").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_4_i5').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".fixed_4_3").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_pro_4_m1').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_41_1").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_41_1_13').val(t2);
  },
  
    
});
  });
}); 

</script>

<!-- End Macbook Pro Silicon (2020 - Current) -->
