<?php include 'layouts/header.php';?>

<section class="ftco-section ftco-no-pb" style="background-image: linear-gradient(to left, #4d774e, #63854c, #7d9248, #9c9e44, #bea843, #c7ae49, #cfb54e, #d8bb54, #c6c060, #b6c46e, #a8c77e, #9dc88d);" data-stellar-background-ratio="0.5">
  <div class="condition-top-section">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <h1 class="mb-4" style="none">It’s easy to sell your Macbook for cash!</h1>
            <p style="font-size: 18px;">In order to offer an accurate price we need to know the age and specification of your macbook</p>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
               <div id="progress-bar" class="row condition-progress-bar">
                  <div class="col-3 cp-box cp-box-process cp-box-active js-active first">
                     <span style="font-size: 18px;">Select Type</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Select Generation</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box">
                     <span style="font-size: 18px;">Condition &amp; Functionality</span>
                     <div class="tick"></div>
                  </div>
                  
                  <div class="col-3 cp-box last">
                     <span style="font-size: 18px;">Confirm offer</span>
                     <div class="tick"></div>
                  </div>
               </div>
            </div>
        </div>
    </div>
  </div>
<!-- ---------------------Type Section Start---------------------- --> 
<section class="flex-grow-1 d-flex flex-column">
    <div class="buttons condition-box">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4 class="mb-4">Select Your Mac Type</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px"  data-toggle="modal" data-target="#js-help-modal" data-href="">
                    <span>How do I find this</span> 
                    <picture>
                        <img src="images/question.png" alt="Help"> 
                    </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php include 'layouts/connection.php';

            $sql = "SELECT * from macbook_ver where mac_id='mac' ";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo "<div class='container'>";
                echo " <div class='row justify-content-center'>";
                while($row = $result->fetch_assoc()) {
                    echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 box2 mb-4' >";
                    if($row['id']=='2') {
                        echo " <a  class='show box mac_type_2' target='2' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    elseif($row['id']=='3'){
                        echo " <a  class='show box mac_type_3' target='3' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    elseif($row['id']=='4'){
                        echo " <a  class='show box mac_type_4' target='4' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    else{
                        echo " <a  class='show box mac_type_1' target='1' data-t_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/macbook.png'>";
                        echo " </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
                echo"</div>";
            }
            ?>
        </div>
    </div>
<!-- --------------------Type Section End---------------------- -->
    
<!-- /////////////////////// Macbook Air (2009 - Late 2010) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div1" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php include 'layouts/connection.php';

            $sql = "SELECT * from macbook_prs WHERE id='1' ";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    
                    echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                    echo "<a  class='show2 box mac_prs_core2' target='11' data-p_id='{$row['id']}'>";
                    echo "<div class='box-image'>";
                    echo "<img src='images/intelcore2.png'  alt='Macbook Intel Core i2'>";
                    echo" </div>";
                    echo "<p style='font-size:18px;'>".trim($row['name'])."</p>";
                    echo "</p>";
                    echo "</form>";
                    echo "</a>";
                    echo "</div>";
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div11" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "sellmacbook";

                $conn = new mysqli($servername, $username, $password, $dbname);

                // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_13' target='111' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>
    <!-- End Screen Section -->
    <!-- Start Model Section -->
    <div class="container">
        <div id="div111" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "sellmacbook";

            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==28){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac1Form'>";
                        echo " <div class='box-image' onclick='mymac1Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_1'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    
    <!-- End Model Section -->
<!-- /////////////////////// End Macbook Air (2009 - Late 2010) ///////////////////////// -->






<!-- /////////////////////// Macbook Metal (2008 - 2012) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div2" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php 
            include 'layouts/connection.php';

            $sql = "SELECT * from macbook_prs WHERE id='1' ";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    
                    echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                    echo "<a  class='show2 box mac_prs_core2' target='21' data-p_id='{$row['id']}'>";
                    echo "<div class='box-image'>";
                    echo "<img src='images/intelcore2.png'  alt='Macbook Intel Core i2'>";
                    echo" </div>";
                    echo "<p style='font-size:18px;'>".trim($row['name'])."</p>";
                    echo "</p>";
                    echo "</form>";
                    echo "</a>";
                    echo "</div>";
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div21" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_13' target='211' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>
    <!-- End Screen Section -->
    <!-- Start Model Section -->
    <div class="container">
        <div id="div211" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==28){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac2Form'>";
                        echo " <div class='box-image' onclick='mymac2Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_2'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    
    <!-- End Model Section -->
<!-- /////////////////////// End Macbook Metal (2008 - 2012) ///////////////////////// -->






<!-- /////////////////////// Macbook White Plastic Unibody (2009 - 2010) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div3" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php 
            include 'layouts/connection.php';

            $sql = "SELECT * from macbook_prs WHERE id='1' ";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    
                    echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                    echo "<a  class='show2 box mac_prs_core2' target='31' data-p_id='{$row['id']}'>";
                    echo "<div class='box-image'>";
                    echo "<img src='images/intelcore2.png'  alt='Macbook Intel Core i2'>";
                    echo" </div>";
                    echo "<p style='font-size:18px;'>".trim($row['name'])."</p>";
                    echo "</p>";
                    echo "</form>";
                    echo "</a>";
                    echo "</div>";
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div31" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==2) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_21_1' target='311' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>
    <!-- End Screen Section -->
    <!-- Start Model Section -->
    <div class="container">
        <div id="div311" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==28){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac3Form'>";
                        echo " <div class='box-image' onclick='mymac3Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_3'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_core2'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_13'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    
    <!-- End Model Section -->
<!-- /////////////////////// End Macbook White Plastic Unibody (2009 - 2010) ///////////////////////// -->




<!-- /////////////////////// Macbook Retina Usb-C (2015 - Current) ///////////////////////// -->
<!-- Processor Section -->
<div class="container">
        <div id="div4" class="targetDiv">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Processor</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-target="#js-help-modal" data-toggle="modal">
                       <span>How do I find this</span>
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
             
            <?php include 'layouts/connection.php';

            $sql = "SELECT * from macbook_prs";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row

                while($row = $result->fetch_assoc()) {
                    if($row['id']==3){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box mac_prs_i5' target='41' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei5.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==2){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box mac_prs_i7' target='42' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorei7.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==7){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box mac_prs_m' target='43' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorem.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==6){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box mac_prs_m3' target='44' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorem3.jpg' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==5){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box mac_prs_m5' target='45' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorem.png' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                    if($row['id']==4){
                        echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                        echo " <a class='show2 box mac_prs_m7' target='46' data-p_id='{$row['id']}'>";
                        echo " <div class='box-image'>";
                        echo " <img src='images/intelcorem7.jpg' alt='Macbook Intel Core i5'>";
                        echo" </div>";
                        echo "<p style='font-size:18px;'>".trim($row['name'])."</p> ";
                        echo "</p>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
            }
            echo "</div>";
            ?>            
            </div>
        </div>
    </div>
    <!-- End Processor Section -->

    <!-- Start Screen Section --> 
    <div class="container">
        <div id="div41" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==6) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_12' target='411' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div42" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==6) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_12' target='421' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div43" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==6) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_12' target='431' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div44" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==6) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_12' target='441' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div45" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==6) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_12' target='451' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="div46" class="targetDivs">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Screen Size</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <div class="container">
             
                <?php
                include 'layouts/connection.php';

                $sql = "SELECT * from macbook_size";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                // output data of each row
                    echo " <div class='row justify-content-center'>";
                    while($row = $result->fetch_assoc()) {
                        if($row['id']==6) {
                            echo "<div class='col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6 px-2 mb-4 text-center' >";
                            echo " <a class='show3 box s_mac_12' target='461' data-s_id='{$row['id']}'>";
                            echo " <div class='box-image'>";
                            echo " <img src='images/macbook-air-11.png' alt='screen size'>";
                            echo" </div>";
                            echo "<p>" .$row['size']."</p> ";
                            echo "</a>";
                            echo "</div>";
                        }
                    }
                    echo "</div>";
                }
            ?>
            </div>
        </div>
    </div>
    <!-- End Screen Section -->
    <!-- Start Model Section -->
    <div class="container">
        <div id="div411" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==30){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac4Form'>";
                        echo " <div class='box-image' onclick='mymac4Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_i5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_12'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div421" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==30){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac5Form'>";
                        echo " <div class='box-image' onclick='mymac5Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_i7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_12'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div431" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==30){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac6Form'>";
                        echo " <div class='box-image' onclick='mymac6Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_m'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_12'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div441" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==30){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac7Form'>";
                        echo " <div class='box-image' onclick='mymac7Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_m3'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_12'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div451" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==30){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac8Form'>";
                        echo " <div class='box-image' onclick='mymac8Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_m5'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_12'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    <div class="container">
        <div id="div461" class="targetDivss">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center process">
                    <h4>Select Your Macbook Model Number</h4>
                    <button type="button" class="green-link js-help-modal" data-max-width="700px" data-href="" data-toggle="modal" data-target="#js-help-modal">
                       <span>How do I find this</span> 
                       <picture>
                          <img src="images/question.png" alt="Help"> 
                       </picture>
                    </button>
                    <br><br>
                </div>
            </div>
            <?php
            include 'layouts/connection.php';

            $sql = "SELECT * from mac_model";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
                echo " <div class='row offset-lg-0 offset-xl-2 col-lg-12 col-xl-8 px-2 d-flex justify-content-lg-center'>";
                while($row = $result->fetch_assoc()) {
                    if($row['id']==30){
                        echo "<div class='col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 px-2 mb-4 text-center' >";
                        echo " <form action='intelprocess.php' class='box' id='mymac9Form'>";
                        echo " <div class='box-image' onclick='mymac9Function()'>";
                        echo " <img src='images/image10.png'  alt='model number'>";
                        echo" </div>";
                        echo "<p>".trim($row['name'])."</p> ";
                        echo  "<input type='hidden' name='mac' class='mac_v_mac_4'/>";
                        echo  "<input type='hidden' name='mac_os' class='mac_p_mac_prs_m7'/>";
                        echo  "<input type='hidden' name='macscr' class='mac_s_mac_12'/>";
                        echo  "<input type='hidden' name='macmod' value='".$row['id']."'/>";
                        echo "</p>";
                        echo"</form>";
                        echo "</a>";
                        echo "</div>";
                    }
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>

    
    <!-- End Model Section -->


<!-- /////////////////////// End Macbook Retina Usb-C (2015 - Current) ///////////////////////// -->




















<!-- Find Serial Number Pop Up starts -->
    <div class="modal fade" id="js-help-modal" role="dialog" tabindex="-1" aria-hidd="true">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content" style="margin-top:120px;">
                <div class="modal-body position-relative" style="overflow-y: auto;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    <div class="container-fluid">
                        <div class="row mx-0">
                            <div class="mb-5">
                                <h1 class="mb-4">How do I find Serial Number - MACBOOK</h1>
                                <p>You can find your devices serial number by one of the following methods.</p>
                                <h4 class="mt-4">If your device powers on:</h4>
                                <p>1. Click the Apple icon at the top left of the screen.</p>
                                <img src="images/how_to_find.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                                <p>2. Click “About This Mac”</p>
                                <img src="images/convertto.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I Find Screen Size"> 
                                <p>3. Your serial number will be stated within the details window. You can copy the serial by highlighting the text and pressing <kbd><kbd>Cmd</kbd> + <kbd>C</kbd></kbd>.</p>
                                <img src="images/xserial.png" class="img-fluid rounded shadow-sm d-block mx-auto mb-4" alt="How Do I find The Serial Number"> 
                                <h4 class="mt-4">If your device does not power on:</h4>
                                <p>1. On the underside of your mac there will be some text below the device type, check for the text “Serial”, the following numbers will be the device serial number.</p>
                                <img src="images/macbookserial.png" class="img-fluid rounded shadow-sm d-block mx-auto" alt="How Do I find The Serial Number"> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- Find Serial Number Pop Up starts -->
</section> 

<?php include 'layouts/footer.php';?>
<script>
//On click of the 'next' anchor
$('.accordion--form__next-btn').on('click touchstart', function() {

  var parentWrapper = $(this).parent().parent();
  var nextWrapper = $(this).parent().parent().next('.accordion--form__fieldset');
  var sectionFields = $(this).siblings().find('.required');


  //Validate the .required fields in this section
  var empty = $(this).siblings().find('.required').filter(function() {

    return this.value === "";

  });

  if (empty.length) {

    $('.accordion--form__invalid').show();

  } else {

    $('.accordion--form__invalid').hide();

    //If valid
    //On the next fieldset -> accordion wrapper, toggle the active class
    nextWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

    //Close the others
    parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

    //Add a class to the parent legend
    nextWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

    //Remove the active class from the other legends
    parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');

  }

  return false;
});

//On click of the 'prev' anchor
$('.accordion--form__prev-btn').on('click touchstart', function() {

  parentWrapper = $(this).parent().parent();
  prevWrapper = $(this).parent().parent().prev('.accordion--form__fieldset');

  //On the prev fieldset -> accordion wrapper, toggle the active class
  prevWrapper.find('.accordion--form__wrapper').addClass('accordion--form__wrapper-active');

  //Close the others
  parentWrapper.find('.accordion--form__wrapper').removeClass('accordion--form__wrapper-active');

  //Add a class to the parent legend
  prevWrapper.find('.accordion--form__legend').addClass('accordion--form__legend-active');

  //Remove the active class from the other legends
  parentWrapper.find('.accordion--form__legend').removeClass('accordion--form__legend-active');


  return false;
});


function sendContact() {
  var valid;  
  valid = validateContact();
  if(valid) {
    jQuery.ajax({
    url: "contact_mail.php",
    data:'userName='+$("#userName").val()+'&userEmail='+$("#userEmail").val()+'&subject='+$("#userName","#subject","#userEmail").val()+'&message='+$("#content").val()+'&content='+$('#html-content-holder').html(),
    type: "POST",
    success:function(data){
    $("#mail-status").html(data);
    },
    error:function (){}
    });
  }
}

function mymacFunction() {
  document.getElementById("mymacForm").submit();
}
function mymac1Function() {
  document.getElementById("mymac1Form").submit();
}
function mymac2Function() {
  document.getElementById("mymac2Form").submit();
}
function mymac3Function() {
  document.getElementById("mymac3Form").submit();
}
function mymac4Function() {
  document.getElementById("mymac4Form").submit();
}
function mymac5Function() {
  document.getElementById("mymac5Form").submit();
}
function mymac6Function() {
  document.getElementById("mymac6Form").submit();
}
function mymac7Function() {
  document.getElementById("mymac7Form").submit();
}
function mymac8Function() {
  document.getElementById("mymac8Form").submit();
}
function mymac9Function() {
  document.getElementById("mymac9Form").submit();
}

function myairbFunction() {
  document.getElementById("myairbForm").submit();
}
function myairbbFunction() {
  document.getElementById("myairbbForm").submit();
}
$('.targetDiv').hide();
$('.show').click(function () {
    $('.targetDiv').hide();
    $('#div' + $(this).attr('target')).show();
});

$('.targetDivs').hide();
$('.show2').click(function () {
    $('.targetDivs').hide();
    $('#div' + $(this).attr('target')).show();
});

$('.targetDivss').hide();
$('.show3').click(function () {
    $('.targetDivss').hide();
    $('#div' + $(this).attr('target')).show();
});
</script>


<!-- Today cript -->
<!-- For Typr -->
<script>
$(document).ready(function()  {
  $(".mac_type_1").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_mac_1').val(t2);
  },
  
    
});
  });
}); 
</script>

<script>
$(document).ready(function()  {
  $(".mac_type_2").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_mac_2').val(t2);
  },
  
    
});
  });
}); 
</script>

<script>
$(document).ready(function()  {
  $(".mac_type_3").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_mac_3').val(t2);
  },
  
    
});
  });
}); 
</script>

<script>
$(document).ready(function()  {
  $(".mac_type_4").click(function() {
    var t2 = $(this).data('t_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_v_mac_4').val(t2);
  },
  
    
});
  });
}); 
</script>

<!-- End For Type -->


<!-- For Processor -->

<script>
$(document).ready(function()  {
  $(".mac_prs_core2").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_core2').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".mac_prs_i5").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_i5').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".mac_prs_i7").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_i7').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".mac_prs_m").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_m').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".mac_prs_m3").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_m3').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".mac_prs_m5").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_m5').val(t2);
  },
 
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".mac_prs_m7").click(function() {
    var t2 = $(this).data('p_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_p_mac_prs_m7').val(t2);
  },
 
});
  });
}); 

</script>

<!-- End For Processor -->

<!-- For Screen -->

<script>
$(document).ready(function()  {
  $(".s_mac_13").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_mac_13').val(t2);
  },
  
    
});
  });
}); 

</script>

<script>
$(document).ready(function()  {
  $(".s_mac_12").click(function() {
    var t2 = $(this).data('s_id');
    console.log(t2);
    $.ajax({
      type: "POST",
     url: "intelprocess.php",
     data: t2,
  success: function(data) {
       $('.mac_s_mac_12').val(t2);
  },
  
    
});
  });
}); 

</script>

<!-- End For Screen -->